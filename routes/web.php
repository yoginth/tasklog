<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::view('/welcome', 'welcome/welcome')->name('welcome');
    Route::view('/telegram', 'welcome/telegram')->name('telegram');
    Route::view('/finish', 'welcome/finish')->name('finish');
    Route::post('/congrats', 'User\Settings\MiscController@updateUser')->name('congrats');
});

Route::get('/', 'HomeController@index')->name('home');
Route::get('/products', 'ProductsController@index');
Route::get('/makers', 'MakersController@index');

// User Routes
Route::group(['prefix' => '@{username}'], function () {
    Route::get('/', 'User\DoneController@done')->name('done');
    Route::get('/products', 'User\ProductsController@products')->name('products');
    Route::get('/pending', 'User\PendingController@pending')->name('pending');
    Route::get('/questions', 'User\QuestionsController@questions')->name('questions');
    Route::get('/answers', 'User\AnswersController@answers')->name('answers');
    Route::get('/stats', 'User\StatsController@stats')->name('stats');
});

// Product Routes
Route::group(['prefix' => 'products/{slug}'], function () {
    Route::get('/', 'Product\OverviewController@overview')->name('product_overview');
    Route::get('/done', 'Product\DoneController@done')->name('product_done');
    Route::get('/pending', 'Product\PendingController@pending')->name('product_pending');
    Route::get('/updates', 'Product\UpdatesController@updates')->name('product_updates');
    Route::get('/questions', 'Product\QuestionsController@question')->name('product_question');
    Route::get('/stats', 'Product\StatsController@stats')->name('product_stats');
});

// Q&A Routes
Route::resource('questions', 'Questions\QuestionController');
Route::resource('unanswered', 'Questions\UnansweredController');
Route::resource('popular', 'Questions\PopularController');
Route::post('questions/{question}/answers', 'Questions\AnswerController@store');
Route::post('/clap/{id}', 'Questions\AnswerController@clap');

// Settings Routes
Route::group(['prefix' => 'settings', 'middleware' => 'auth'], function () {
    Route::resource('/profile', 'User\Settings\ProfileController', ['only' => ['index', 'update']]);
    Route::resource('/account', 'User\Settings\AccountController', ['only' => ['index', 'update']]);
    Route::resource('/social', 'User\Settings\SocialController', ['only' => ['index', 'update']]);
    Route::resource('/password', 'User\Settings\PasswordController', ['only' => ['index', 'update']]);
    Route::resource('/labs', 'User\Settings\LabsController', ['only' => ['index', 'update']]);
});

// Admin Routes
Route::middleware(['admin'])->group(function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::view('/', 'admin/admin')->name('admin');
        Route::post('/suspend/{id}', 'Admin\SuspendController@suspend');
    });
});

// API Routes
Route::middleware(['auth'], 'throttle:5, 1')->group(function () {
    // Tasks API Routes
    Route::resource('api/tasks', 'TaskController', ['only' => ['store', 'update', 'destroy']]);

    // Dark Mode Route
    Route::post('/darkmode', 'User\Settings\MiscController@darkMode')->name('dark_mode')->middleware('patron');
});

Route::get('api/task/{id}', 'TaskController@get');
Route::get('api/question/{id}', 'Questions\QuestionController@get');
Route::get('api/product/{slug}', 'ProductsController@get');

Route::get('popover/products/{id}', 'ProductsController@popover');

// Pages Routes
Route::get('/terms', 'PagesController@terms');
Route::get('/guidelines', 'PagesController@guidelines');
Route::get('/api', 'PagesController@api');
Route::get('/deals', 'PagesController@deals');
Route::get('/help', 'PagesController@help');
Route::get('/meetups', 'PagesController@meetups');
Route::get('/patron', 'PagesController@patron');
Route::get('/widgets', 'PagesController@widgets');
Route::get('/open', 'PagesController@open');
Route::get('/beta', 'PagesController@beta')->middleware('beta');

Route::get('/patron', function () {
    return view('pages.patron');
});

Route::middleware(['auth'])->group(function () {
    Route::post('/dopay', 'PatronController@handleonlinepay');
});

// Sitemap Routes
Route::group(['prefix' => 'sitemap'], function () {
    Route::get('/users.xml', 'Sitemap\UserSitemapController@index');
});
