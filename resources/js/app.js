require('./bootstrap');
var Turbolinks = require("turbolinks")
Turbolinks.start()

var token = $('meta[name="csrf-token"]').attr('content')

// Done Status Localstorage
$(document).on('turbolinks:load', function() {
  var data = localStorage.donestatus;
  $("input[name='donestatus']")
    .prop('checked', data === 'true')
    .change(function () {
       localStorage.donestatus = $(this).prop("checked");
    });
});

// Mark Task as Completed
$(document).on('turbolinks:load', function() {
	$('[name="checkbox"]').change(function() {
		var key = $(this).val()
		var value = $(this).is(":checked")

		var patch = {
			"done" : value,
		}

		$.ajax({
			headers: { 'X-CSRF-TOKEN': token },
			type: 'PATCH',
			data: JSON.stringify(patch),
			url: '/api/tasks/' + key,
			contentType: 'application/merge-patch+json',
		})
		.done(function(data)
		{
			$('#next-up-' + data.id).remove();
			$('#done-' + data.id).remove();
		});
	});
});

// Delete Task
$(document).on('turbolinks:load', function() {
	$('[name="deletetask"]').on('click', function () {
		var key = $(this).val()

		var data = {
			"delete" : true,
			_method: 'DELETE'
		}
		$.ajax({
			headers: { 'X-CSRF-TOKEN': token },
			type: 'DELETE',
			data: JSON.stringify(data),
			url: '/api/tasks/' + key,
		})
		.done(function()
		{
			$('#next-up-' + key).remove();
			$('#done-' + key).remove();
		});
	});
});

// Infinite Scroll
var page = 1;
$(window).scroll(function() {
	if($(window).scrollTop() + $(window).height() + 1 >= $(document).height()) {
		page++;
		loadMoreData(page);
	}
});

function loadMoreData(page){
	$.ajax({
		url: '?page=' + page,
		type: "GET",
		beforeSend: function()
		{
			$('.ajax-load').show();
		}
	})
	.done(function(data) {
		if(data.html === ""){
			$('.ajax-load').html("No more records found");
			return;
		}
		$('.ajax-load').hide();
		$("#post-data").append(data.html);
	})
	.fail(function() {
		$('.ajax-load').html("Something went Wrong!");
	});
}

// Product Popover
$(document).on('turbolinks:load', function() {
	$('.hashtag').popover({
		placement: "top",
		content: productDetails,
		trigger: 'hover',
		html: true
	}); 
});
  
// Get user details for tooltip
function productDetails() {
	var href = this.href;

	var tooltipText = "";
	$.ajax({
		url: '/popover' + RemoveBaseUrl(href),
		type: 'GET',
		async: false,
		success: function(response) {
			tooltipText = response;
		},
		error: function() { 
			tooltipText = "🤷";
		}
	});
	return tooltipText;
}

function RemoveBaseUrl(url) {
    var baseUrlPattern = /^https?:\/\/[a-z\:0-9.]+/;
    var result = "";
 
    var match = baseUrlPattern.exec(url);
    if (match !== null) {
        result = match[0];
    }
 
    if (result.length > 0) {
        url = url.replace(result, "");
    }
 
    return url;
}
