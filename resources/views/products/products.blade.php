@extends('layouts.app')

@section('content')

<div class="container-fluid">
	@guest @include('layouts.banner') @endguest
	<div class="row justify-content-center">
		@include('layouts.sidenav')
		<div class="col-md-7 @guest mt-4 @endguest" id="post-data">
			@include('products.list')
			<div class="ajax-load text-center" style="display:none">
				<p>Loading More post</p>
			</div>
		</div>
		@include('products.sidebar')
	</div>
</div>

@endsection
