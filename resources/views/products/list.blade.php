@foreach ($dates as $date)
    <h6 class="mb-3">
        <span class="font-weight-bold">{{ $date[0]->created_at->format('l') }}</span>,
        <span class="font-weight-normal">{{ $date[0]->created_at->format('F j, Y') }}</span>
    </h6>
    <div class="card todo mb-3">
        @foreach (App\Product::whereDate('created_at', $date[0]->created_at->format('Y-m-d'))
                    ->limit(5)
                    ->get() as $product)
            <li class="list-group-item">
                <img class="avatar small rounded mr-2" src="{{ $product->logo }}">
                <a class="text-dark" href="{{ route('product_overview', [$product->slug]) }}">
                    <span class="font-weight-bold">{{ $product->name }}</span>
                </a>
                <span>{{ $product->pitch }}</span>
                <a href="{{ route('done', [$product->user->username]) }}">
                    <img class="avatar small rounded-circle float-right" src="{{ $product->user->avatar }}">
                </a>
                {{-- Add Follow Button --}}
            </li>
        @endforeach
    </div>
@endforeach
