@extends('layouts/app')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        @include('layouts.sidenav')
        <div class="col-md-7 @guest mt-4 @endguest">
            <div class="card">
                <div class="card-body">
                    <p>Asked {{ $question->created_at->diffForHumans() }}</p>
                    <h4>{{ $question->title }}</h4>
                    <p class="card-text">@markdown($question->body)</p>
                </div>
            </div>
            @foreach($question->answers as $answer)
                @include('questions.answer')
            @endforeach
            <div class="mb-4"></div>
            @guest
                <div class="card text-center bg-light mb-4">
                    <div class="card-body">
                        <a class="text-dark text-decoration-none text-uppercase" href="/login">Sign in to answer 😃</a>
                    </div>
                </div>
            @else
                @if (Auth::check() and !Auth::user()->isSuspended())
                    <h4>Answer</h4>
                    <form method="POST" action="{{ $question->path() . "/answers" }}">
                        @csrf
                        <div class="form-group">
                            <textarea name="body" id="body" class="form-control" placeholder="Write your answer here" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-success">Post</button>
                    </form>
                    <div class="mb-4"></div>
                @else
                    @include('layouts.suspended')
                @endif
            @endguest
        </div>
        @include('questions.questionsidebar')
    </div>
</div>

@endsection
