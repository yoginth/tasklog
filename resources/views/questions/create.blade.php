@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        @include('layouts.sidenav')
        <div class="col-md-7">
            <h5 class="mb-3">Ask a new question</h5>

            <div class="card p-4">
                <form method="POST" action="/questions">
                    @csrf
                    <div class="form-group">
                        <label class="h6" for="title">Title</label>
                        <input type="text" class="form-control" name="title">
                    </div>

                    <div class="form-group">
                        <label class="h6" for="body">Body</label>
                        <textarea name="body" id="body" class="form-control" rows="8"></textarea>
                    </div>

                    <div class="form-group">
                        <label class="h6" for="body">Tags</label>
                        <input type="text" class="form-control" name="tags" placeholder="Technology, Web">
                    </div>

                    <button type="submit" class="btn btn-primary">Publish</button>
                </form>

            </div>
        </div>
        @include('questions.sidebar')
    </div>
</div>
@endsection
