<div class="card mt-3 mb-n2">
    <div class="card-body">
        <a href="/questions/{{ $answer->question->id }}#answer-{{ $answer->id }}" id="answer-{{ $answer->id }}">
            <small class="float-right">{{ $answer->created_at->diffForHumans() }}</small>
        </a>
        <div class="mb-2">
            <img src="{{ $answer->user->avatar }}" class="avatar small rounded-circle">
            <span class="name ml-1 font-weight-bold vamiddle">
                @if ($answer->user->name)
                    <a class="text-dark" href="{{ route('done', [$answer->user->username]) }}">
                        {{ $answer->user->name }}
                    </a>
                @else
                    <a class="text-dark" href="{{ route('done', [$answer->user->username]) }}">
                        {{ $answer->user->username }}
                    </a>
                @endif
            </span>
        </div>
        <p class="card-text">@markdown($answer->body)</p>
        <form method="POST" action="{{ "/clap/" . $answer->id }}">
            @csrf
            <button type="submit" class="btn btn-outline-secondary btn-sm like">👏 {{ $answer->claps }}</button>
        </form>
    </div>
</div>
