@inject('check', 'App\Http\Controllers\Service\Service')

<div class="col-md-auto">
    <div class="card @guest mt-4 @endguest" style="width: 18rem;">
		<div class="card-header">Asked by</div>
		<div class="card-body">
			<img src="{{ $question->user->avatar }}" class="avatar small rounded-circle">
            <span class="name ml-1 font-weight-bold">
                <a class="ml-1 h6 text-dark text-decoration-none" href="{{ route('done', [$question->user->username]) }}">
                    @if ($question->user->firstname)
                        {{ $question->user->firstname }} {{ $question->user->lastname }}
                    @else
                        {{ $question->user->username }}
                    @endif
                </a>
                @if ($question->user->is_patron)
                    <span class="badges patron vamiddle ml-1">{{ Emoji::gemStone() }}</span>
                @endif
                @if ($question->user->streak > 0)
                    <span class="badges streaks vamiddle ml-1">{{ Emoji::fire() }} {{ $question->user->streak }}</span>
                @endif
                @if ($check->isNewUser($question->user->created_at))
                    <span class="badges new text-uppercase vamiddle ml-1">New</span>
                @endif
            </span>
		</div>
	</div>

    @if ($question->tags->count())
        <div class="card mt-3" style="width: 18rem;">
            <div class="card-header">Tags</div>
            <div class="card-body">
                @foreach ($question->tags as $tag)
                    <a href="#" class="btn btn-light2 btn-sm">{{ $tag['name'] }}</a>
                @endforeach
            </div>
        </div>
    @endif
</div>
