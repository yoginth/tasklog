<div class="col-md-auto">
    <div class="card text-white bg-warning @guest mt-4 @endguest" style="max-width: 18rem;">
        <div class="card-body">
            <h3 class="card-title">
                All your questions answered.
            </h3>
            <a href="/questions/create" class="btn btn-dark font-weight-bold">
                {{ Emoji::backhandIndexPointingRight() }} Ask a Question
            </a>
        </div>
    </div>

    <div class="card mt-3" style="max-width: 18rem;">
        <div class="card-body">
            @foreach ($questions as $question)
                @foreach ($question->tags as $tag)
                    <a href="#" class="btn btn-light2 btn-sm mb-2 mr-1">{{ $tag['name'] }}</a>
                @endforeach
            @endforeach
        </div>
    </div>
</div>
