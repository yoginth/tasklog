@extends('layouts/app')

@section('content')

<div class="container-fluid">
    @guest
        @include('layouts.banner')
    @endguest
    <div class="row justify-content-center">
        @include('layouts.sidenav')
        <div class="col-md-7 @guest mt-4 @endguest">
            @include('questions.nav')
            @if ($questions->count())
                @foreach($questions as $question)
                    @if ($question->answers->count() == 0)
                        <div class="card todo">
                            <div class="list-group-item header">
                                <img src="{{ $question->user->avatar }}" class="avatar small rounded-circle">
                                <span class="name ml-1">
                                    @if ($question->user->firstname)
                                        <a class="text-dark" href="{{ route('done', [$question->user->username]) }}">
                                            {{ $question->user->firstname }} {{ $question->user->lastname }}
                                        </a>
                                    @else
                                        <a class="text-dark" href="{{ route('done', [$question->user->username]) }}">
                                            {{ $question->user->username }}
                                        </a>
                                    @endif
                                    @if ($question->user->is_patron)
                                        <span class="badges patron vamiddle ml-1">{{ Emoji::gemStone() }} PATRON</span>
                                    @endif
                                    @if ($question->user->streaks > 0)
                                        <span class="badges streaks vamiddle ml-1">{{ Emoji::fire() }} {{ $question->user->streaks }}</span>
                                    @endif
                                </span>
                            </div>
                            <li class="list-group-item">
                                <h5><a class="text-dark" href='{{ $question->path() }}'>{{ $question->title }}</a></h5>
                                <div class='body'>
                                    {{ $question->body }}
                                </div>
                                <a class="btn btn-light2 btn-sm mt-2" href="{{ $question->path() }}">
                                    @if ($question->answers->count())
                                        @if ($question->answers->count() == 1)
                                            {{ $question->answers->count() }} answer
                                        @else
                                            {{ $question->answers->count() }} answers
                                        @endif
                                    @else
                                        Be the first to answer
                                    @endif
                                </a>
                            </li>
                        </div>
                    @endif
                    <div class="mb-3"></div>
                @endforeach
            @else
                @include('questions.empty')
            @endif
        </div>
        @include('questions.sidebar')
    </div>
</div>

@endsection
