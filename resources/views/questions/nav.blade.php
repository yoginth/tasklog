<ul class="nav nav-pills justify-content-center mb-3">
    <li class="nav-item">
        <a class="nav-link {{ Request::is('questions') ? 'active' : '' }}" href="/questions">Newest</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Request::is('unanswered') ? 'active' : '' }}" href="/unanswered">Unanswered</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Request::is('popular') ? 'active' : '' }}" href="/popular">Popular</a>
    </li>
</ul>
