<div class="mt-4">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title">2,029 Completed Todos</h5>
			<canvas id="completed" style="width:100%" height="300"></canvas>
		</div>
		<div class="card-body">
			<h5 class="card-title">Completed Todos by day of week</h5>
			<canvas id="completed-week" style="width:100%" height="300"></canvas>
		</div>
		<div class="card-body">
			<h5 class="card-title">57 Questions</h5>
			<canvas id="questions" style="width:100%" height="300"></canvas>
		</div>
		<div class="card-body">
			<h5 class="card-title">372 Answers</h5>
			<canvas id="answers" style="width:100%" height="300"></canvas>
		</div>
		<div class="card-body">
			<h5 class="card-title">24 Products</h5>
		</div>
	</div>
	<div class="mb-3"></div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>

<script>
	// Completed
	new Chart(document.getElementById("completed"), {
		type: 'bar',
		data: {
			labels: {{ $burndown }},
			datasets: [
			{
				backgroundColor: "#3e95cd",
				data: {{ $burndown_tasks_count }},
			}
			]
		},
		options: {
			legend: { display: false },
			animation: false,
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});

	// Completed Week
	new Chart(document.getElementById("completed-week"), {
		type: 'bar',
		data: {
			labels: ["1", "2", "3", "4", "5"],
			datasets: [
			{
				backgroundColor: "#3e95cd",
				data: [10, 20, 12, 21, 10],
			}
			]
		},
		options: {
			legend: { display: false },
			animation: false,
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});

	// Questions
	new Chart(document.getElementById("questions"), {
		type: 'bar',
		data: {
			labels: ["1", "2", "3", "4", "5"],
			datasets: [
			{
				backgroundColor: "#3e95cd",
				data: [10, 20, 12, 21, 10],
			}
			]
		},
		options: {
			legend: { display: false },
			animation: false,
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});

	// Answers
	new Chart(document.getElementById("answers"), {
		type: 'bar',
		data: {
			labels: ["1", "2", "3", "4", "5"],
			datasets: [
			{
				backgroundColor: "#3e95cd",
				data: [10, 20, 12, 21, 10],
			}
			]
		},
		options: {
			legend: { display: false },
			animation: false,
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});
</script>
