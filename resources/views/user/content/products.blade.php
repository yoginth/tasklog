@foreach ($user->products as $product)
<div class="col-sm-6 mb-3">
	<div class="card">
		<div class="row no-gutters align-items-center">
			<div class="col-3">
				<img class="avatar mb-3 ml-3 mt-3 product rounded" src="{{ $product->logo }}">
			</div>
			<div class="col">
				<div class="card-block mr-3">
					<a class="text-dark" href="{{ route('product_overview', [$product->slug]) }}">
						<h4>{{ $product->name }}</h4>
					</a>
					<span>{{ $product->pitch }}</span>
				</div>
			</div>
		</div>
	</div>
</div>
@endforeach
