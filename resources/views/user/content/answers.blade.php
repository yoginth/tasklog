@foreach($user->answers as $answer)
    <div class="card todo">
        <div class="list-group-item header">
            <span class="name ml-1">
                @if ($answer->user->name)
                    <a class="text-dark" href="{{ $answer->question->path() }}">
                        {{ $answer->question->title }}
                    </a>
                @else
                    <a class="text-dark" href="{{ $answer->question->path() }}">
                        {{ $answer->question->title }}
                    </a>
                @endif
            </span>
        </div>
        <li class="list-group-item">
            <small class="float-right">{{ $answer->created_at->diffForHumans() }}</small>
            <div class="mb-2">
                <img src="{{ $answer->user->avatar }}" class="avatar small rounded-circle">
                <span class="name ml-1 font-weight-bold vamiddle">
                    @if ($answer->user->name)
                        <a class="text-dark" href="{{ route('done', [$answer->user->username]) }}">
                            {{ $answer->user->name }}
                        </a>
                    @else
                        <a class="text-dark" href="{{ route('done', [$answer->user->username]) }}">
                            {{ $answer->user->username }}
                        </a>
                    @endif
                </span>
            </div>
            <p class="card-text">{{ $answer->body }}</p>
            <form method="POST" action="{{ "/clap/" . $answer->id }}">
                @csrf
                <button type="submit" class="btn btn-outline-secondary btn-sm like">👏 {{ $answer->claps }}</button>
            </form>
        </li>
    </div>
    <div class="mb-3"></div>
@endforeach
