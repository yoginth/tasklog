@foreach($user->questions as $question)
    <div class="card todo">
        <div class="list-group-item header">
            <img src="{{ $question->user->avatar }}" class="avatar small rounded-circle">
            <span class="name ml-1">
                @if ($question->user->name)
                    <a class="text-dark" href="{{ route('done', [$question->user->username]) }}">
                        {{ $question->user->name }}
                    </a>
                @else
                    <a class="text-dark" href="{{ route('done', [$question->user->username]) }}">
                        {{ $question->user->username }}
                    </a>
                @endif
            </span>
        </div>
        <li class="list-group-item">
            <h5><a class="text-dark" href="{{ $question->path() }}">{{ $question->title }}</a></h5>
            <div class='body'>
                {{ $question->body }}
            </div>
            <a class="btn btn-light2 btn-sm mt-2" href="{{ $question->path() }}">
                @if ($question->answers->count())
                    {{ $question->answers->count() }} answers
                @else
                    Be the first to answer
                @endif
            </a>
        </li>
    </div>
    <div class="mb-3"></div>
@endforeach
