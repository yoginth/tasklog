@extends('layouts/app')

@section('content')

<div class="container-fluid">
	<div class="row justify-content-center">
		@include('layouts.sidenav')
		<div class="col-md-7 @guest mt-4 @endguest">
			@include('user.summary')
			<div class="mt-4">
				@if ($user->questions->count())
			    	@include('user.content.questions')
			    @else
					@include('layouts.empty')
				@endif
			</div>
		</div>
		@include('user.sidebar')
	</div>
</div>

@endsection
