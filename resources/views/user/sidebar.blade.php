<div class="col-md-auto">
	<div class="card @guest mt-4 @endguest" style="width: 18rem;">
	    <div class="card-body">
	    	@if ($user->telegram)
	        	<a href="https://t.me/{{ $user->telegram }}" class="btn btn-success w-100 mb-2">
	        		<span>@</span>{{ $user->telegram }}
	        	</a>
	        @endif
	        <a href="#" class="btn btn-light2 w-100">
	        	Ask a Question
	        </a>
	        @auth
		        <a href="/settings/profile" class="btn btn-light2 w-100 mt-2">
		        	Edit profile
		        </a>
	    	@endauth
	    </div>
	</div>
	@if ($user->website or $user->telegram or $user->twitter or $user->producthunt)
		<div class="card mt-3" style="width: 18rem;">
			<div class="card-header">
				Around the web
			</div>
			<div class="card-body p-3">
				@if($user->website)
					<img class="social-icon rounded" src="https://www.google.com/s2/favicons?domain={{ $user->website }}">
					<a class="text-dark vamiddle ml-1" href="{{ $user->website }}" class="mb-2" target="_blank">Website</a>
					<br>
				@endif
				@if($user->telegram)
					<img class="social-icon" src="/images/icons/telegram.svg">
					<a class="text-dark vamiddle ml-1" href="https://t.me/{{ $user->telegram }}" target="_blank">Telegram</a>
					<br>
				@endif
				@if($user->twitter)
					<img class="social-icon" src="/images/icons/twitter.svg">
					<a class="text-dark vamiddle ml-1" href="https://twitter.com/{{ $user->twitter }}" target="_blank">Twitter</a>
					<br>
				@endif
				@if($user->producthunt)
					<img class="social-icon" src="/images/icons/producthunt.svg">
					<a class="text-dark vamiddle ml-1" href="https://www.producthunt.com/{{ '@' . $user->producthunt }}" target="_blank">ProductHunt</a>
					<br>
				@endif
			</div>
		</div>
	@endif
	@if (Auth::check() and Auth::user()->isAdmin())
		<div class="card bg-light mt-3" style="width: 18rem;">
			<div class="card-header">
				Admin {{ Emoji::shield() }}
			</div>
		    <div class="card-body">
		    	<div class="mb-2"><span class="font-weight-bold">User ID:</span> {{ $user->id }}</div>
		    	<div class="mb-2"><span class="font-weight-bold">IP Address:</span>
		    		<a href="https://ipinfo.io/{{ $user->ip_address }}" class="text-decoration-none">
		    			{{ $user->ip_address }}
		    		</a>
		    	</div>
		    	<form method="POST" action="{{ "/admin/suspend/" . $user->id }}">
					@csrf
					<div class="form-check mb-2">
						<input class="form-check-input" type="checkbox" id="suspended" name="suspended" @if ($user->isSuspended()) checked @endif>
						<label class="form-check-label mb-1" for="suspended">
							Suspend this user {{ Emoji::hammer() }}
						</label>
					</div>
					<button type="submit" class="btn btn-sm btn-primary">Save</button>
				</form>
		    </div>
		</div>
	@endif
</div>
