@extends('layouts/app')

@section('content')

<div class="container-fluid">
	<div class="row ml-4 mr-4">
		@include('layouts.sidenav')
		<div class="col-md-7 mt-4 settings">
			@include('user.settings.navbar')
			@if (Auth::user()->isBeta())
				<h4>You Enrolled to Beta {{ Emoji::partyPopper() }}</h4>
			@else
				<h4>Enroll to Beta {{ Emoji::testTube() }}</h4>
			@endif
			<form method="POST" action="/settings/labs/{{ Auth::user()->id }}">
				@csrf
				@method('PATCH')
				<div class="form-check mt-4 mb-4">
					<input class="form-check-input" type="checkbox" id="beta" name="beta" @if (Auth::user()->isBeta()) checked @endif>
					<label class="form-check-label" for="beta">
						Opt-in
					</label>
				</div>
				<button type="submit" class="btn btn-primary">Save</button>
			</form>
		</div>
	</div>
</div>

@endsection
