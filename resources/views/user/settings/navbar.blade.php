<ul class="nav nav-pills mb-3">
	<li class="nav-item">
		<a class="nav-link {{ Request::is('settings/profile') ? 'active' : '' }}" href="/settings/profile">
			Profile
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link {{ Request::is('settings/account') ? 'active' : '' }}" href="/settings/account">
			Account
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link {{ Request::is('settings/social') ? 'active' : '' }}" href="/settings/social">
			Social
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link {{ Request::is('settings/password') ? 'active' : '' }}" href="/settings/password">
			Password
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link {{ Request::is('settings/labs') ? 'active' : '' }}" href="/settings/labs">
			Labs
		</a>
	</li>
</ul>
