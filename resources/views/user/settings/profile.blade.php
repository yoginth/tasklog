@extends('layouts/app')

@section('content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.min.js" defer data-turbolinks-track="true"></script>
<div class="container-fluid">
	<div class="row ml-4 mr-4">
		@include('layouts.sidenav')
		<div class="col-md-7 mt-4 settings">
			@include('user.settings.navbar')
			<form method="POST" action="/settings/profile/{{ Auth::user()->id }}" enctype="multipart/form-data">
				@csrf
				@method('PATCH')
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="firstname">First Name</label>
						<input type="text" class="form-control @error('firstname') is-invalid @enderror" id="firstname" name="firstname" value="{{ Auth::user()->firstname }}" placeholder="First Name">
						@error('firstname')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<div class="form-group col-md-6">
						<label for="lastname">Last Name</label>
						<input type="text" class="form-control @error('lastname') is-invalid @enderror" id="lastname" name="lastname" value="{{ Auth::user()->lastname }}" placeholder="Last Name">
						@error('lastname')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
				</div>
				<div class="form-group">
					<label for="location">Location</label>
					<input type="text" class="form-control @error('location') is-invalid @enderror" id="location" name="location" value="{{ Auth::user()->location }}" placeholder="Location">
					@error('location')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<div class="form-group">
					<label for="bio">Bio</label>
					<textarea class="form-control @error('bio') is-invalid @enderror" id="bio" name="bio" rows="3">{{ Auth::user()->bio }}</textarea>
					@error('bio')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<div class="form-group">
					<label for="accent">Accent Color</label>
					<input type="text" class="jscolor {hash:true} form-control @error('accent') is-invalid @enderror" id="accent" name="accent" value="{{ Auth::user()->accent }}" placeholder="#47E0A0">
					@error('accent')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<div class="form-group">
					<label for="pattern_id">Pattern ID</label>
					<input type="text" class="form-control @error('pattern_id') is-invalid @enderror" id="pattern_id" name="pattern_id" value="{{ Auth::user()->pattern_id }}" placeholder="1 to 9">
					@error('pattern_id')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<div class="form-group">
					<label for="avatar">Avatar</label>
					<input class="form-control-file" name="avatar" id="avatar" type="file">
				</div>
				<div class="form-group">
					<label for="cover">Cover</label>
					<input class="form-control-file" name="cover" id="cover" type="file">
				</div>
				<button type="submit" class="btn btn-primary">Save</button>
			</form>
		</div>
	</div>
</div>

@endsection