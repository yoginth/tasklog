@extends('layouts/app')

@section('content')

<div class="container-fluid">
	<div class="row ml-4 mr-4">
		@include('layouts.sidenav')
		<div class="col-md-7 mt-4 settings">
			@include('user.settings.navbar')
			<form method="POST" action="/settings/password/{{ Auth::user()->id }}">
				@csrf
				@method('PATCH')
				<div class="form-group">
					<label for="password">New Password</label>
					<input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="New Password">
					@error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
				</div>
				<div class="form-group">
					<label for="password-confirm">Confirm Password</label>
					<input type="password" class="form-control" id="password-confirm" name="password_confirmation" placeholder="Confirm Password" required>
				</div>
				<button type="submit" class="btn btn-primary">Save</button>
			</form>
		</div>
	</div>
</div>

@endsection
