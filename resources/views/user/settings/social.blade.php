@extends('layouts/app')

@section('content')

<div class="container-fluid">
	<div class="row ml-4 mr-4">
		@include('layouts.sidenav')
		<div class="col-md-7 mt-4 settings">
			@include('user.settings.navbar')
			<form method="POST" action="/settings/social/{{ Auth::user()->id }}">
				@csrf
				@method('PATCH')
				<div class="form-group">
					<label for="username">Website</label>
					<input type="url" class="form-control @error('website') is-invalid @enderror" id="website" name="website" value="{{ Auth::user()->website }}" placeholder="Website">
					@error('website')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>

				<div class="form-group">
					<label for="username">Telegram</label>
					<input type="text" class="form-control @error('telegram') is-invalid @enderror" id="telegram" name="telegram" value="{{ Auth::user()->telegram }}" placeholder="Telegram">
					@error('telegram')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>

				<div class="form-group">
					<label for="username">Twitter</label>
					<input type="text" class="form-control @error('twitter') is-invalid @enderror" id="twitter" name="twitter" value="{{ Auth::user()->twitter }}" placeholder="Twitter">
					@error('twitter')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>

				<div class="form-group">
					<label for="username">ProductHunt</label>
					<input type="text" class="form-control @error('producthunt') is-invalid @enderror" id="producthunt" name="producthunt" value="{{ Auth::user()->producthunt }}" placeholder="ProductHunt">
					@error('producthunt')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<button type="submit" class="btn btn-primary">Save</button>
			</form>
		</div>
	</div>
</div>

@endsection
