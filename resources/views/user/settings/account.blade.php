@extends('layouts/app')

@section('content')

<div class="container-fluid">
	<div class="row ml-4 mr-4">
		@include('layouts.sidenav')
		<div class="col-md-7 mt-4 settings">
			@include('user.settings.navbar')
			<form method="POST" action="/settings/account/{{ Auth::user()->id }}">
				@csrf
				@method('PATCH')
				<div class="form-group">
					<label for="username">Username</label>
					<input type="text" class="form-control @error('username') is-invalid @enderror" id="username" name="username" value="{{ Auth::user()->username }}" placeholder="Username">
					@error('username')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<div class="form-group">
					<label for="email">E-Mail</label>
					<input type="email" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}" placeholder="E-Mail">
				</div>
				<button type="submit" class="btn btn-primary">Save</button>
			</form>
		</div>
	</div>
</div>

@endsection
