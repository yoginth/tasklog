<div class="card">
	@if ($user->cover)
	<div class="user-header" style="background-image:url({{ $user->cover }});background-color:{{ $user->accent }}">
	@else
	<div class="user-header" style="background-image:url(https://tasklog.netlify.com/patterns/{{ $user->pattern_id }}.svg);background-color:{{ $user->accent }}">
	@endif
		<div class="card mt-4 mb-4 ml-3 w-45">
		    <div class="row no-gutters mb-2">
		        <div class="col-auto">
		            <img class="avatar profile rounded-circle mb-3 ml-3 mt-3" src="{{ $user->avatar }}">
		        </div>
		        <div class="col">
		            <div class="card-block ml-3 mr-3 mt-2">
						<h3 class="card-title">
							{{ $user->firstname }} {{ $user->lastname }}
							@if ($user->is_patron)
								<span class="badges patron-profile">{{ Emoji::gemStone() }}</span>
							@endif
						</h3>
		                <div class="card-text h6">{{ $user->bio }}</div>
		            </div>
		        </div>
		    </div>
			<div class="card-footer text-muted profile-footer">
				<div class="row">
					<div class="@if ($user->location) col-sm-3 @else col-sm-4 @endif text-center v-divider"
						title="{{ Emoji::fire() }} {{ $user->streak }} days streak">
						{{ Emoji::fire() }} {{ $user->streak }}
					</div>
				    <div class="@if ($user->location) col-sm-3 @else col-sm-4 @endif text-center v-divider"
				    	title="{{ Emoji::ghost() }} {{ $user->best_streak }} days best streak">
				    	{{ Emoji::ghost() }} {{ $user->best_streak }}
				    </div>
				    @if ($user->location)
					    <div class="col-sm-3 text-center v-divider"
					    	title="{{ Emoji::roundPushpin() }} {{ $user->location }} days streak">
							{{ Emoji::roundPushpin() }} {{ $user->location }}
					    </div>
				    @endif
				    <div class="@if ($user->location) col-sm-3 @else col-sm-4 @endif text-center"
				    	title="{{ Emoji::calendar() }} Joined {{ $user->created_at->format('F, Y') }}">
				    	{{ Emoji::calendar() }} {{ $user->created_at->format('M') }}
				    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="card-footer w-100 text-muted summary-nav">
		<ul class="nav nav-pills nav-fill card-header-pills h6">
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('@' . $user->username) ? 'bg-secondary active' : '' }}" href="{{ route('done', [$user->username]) }}">
					{{ $user->tasks->where('done', 1)->count() }} Done
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('@' . $user->username . '/products') ? 'bg-secondary active' : '' }}" href="{{ route('products', [$user->username]) }}">
					{{ $user->products->count() }} Products
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('@' . $user->username . '/pending') ? 'bg-secondary active' : '' }}" href="{{ route('pending', [$user->username]) }}">
					{{ $user->tasks->where('done', 0)->count() }} Pending
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('@' . $user->username . '/questions') ? 'bg-secondary active' : '' }}" href="{{ route('questions', [$user->username]) }}">
					{{ $user->questions->count() }} Questions
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('@' . $user->username . '/answers') ? 'bg-secondary active' : '' }}" href="{{ route('answers', [$user->username]) }}">
					{{ $user->answers->count() }} Answers
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('@' . $user->username . '/stats') ? 'bg-secondary active' : '' }}" href="{{ route('stats', [$user->username]) }}">
					Stats
				</a>
			</li>
		</ul>
    </div>
</div>
