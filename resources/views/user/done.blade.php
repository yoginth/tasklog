@extends('layouts/app')

@section('content')

<div class="container-fluid">
	<div class="row justify-content-center">
		@include('layouts.sidenav')
		<div class="col-md-7 @guest mt-4 @endguest">
			@include('user.summary')
			<div class="mt-4">
					@auth
						@include('tasks.create')
					@endauth
					@if (Auth::check() and !Auth::user()->isSuspended())
						@if ($user->tasks->where('done', 1)->count())
							<div id="post-data">
								@include('user.content.done')
								<div class="ajax-load text-center" style="display:none">
									<p>Loading More post</p>
								</div>
							</div>
						@else
							@include('layouts.empty')
						@endif
					@endif
				<div class="mb-3"></div>
			</div>
		</div>
		@include('user.sidebar')
	</div>
</div>

@endsection
