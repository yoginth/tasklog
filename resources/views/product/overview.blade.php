@extends('layouts/app')

@section('content')

<div class="container-fluid">
	<div class="row justify-content-center">
		@include('layouts.sidenav')
		<div class="col-md-7 @guest mt-4 @endguest">
			@include('product.summary')
			<div class="mt-4">
					@auth
						@include('tasks.create')
					@endauth
					<ul class="list-group todo" id="post-data">
						@if ($tasks->count())
							@include('product.content.done')
							<div class="ajax-load text-center" style="display:none">
								<p>Loading More post</p>
							</div>
						@else
							@include('layouts.empty')
						@endif
					</ul>
				<div class="mb-3"></div>
			</div>
		</div>
		@include('product.sidebar')
	</div>
</div>

@endsection
