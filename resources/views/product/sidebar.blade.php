@inject('check', 'App\Http\Controllers\Service\Service')

<div class="col-md-auto">
	<div class="card @guest mt-4 @endguest" style="width: 18rem;">
		<div class="card-header">Made by</div>
		<div class="card-body">
			<img src="{{ $product->user->avatar }}" class="avatar small rounded-circle">
            <span class="name ml-1 font-weight-bold">
                <a class="ml-1 h6 text-dark text-decoration-none" href="{{ route('done', [$product->user->username]) }}">
					@if ($product->user->firstname)
						{{ $product->user->firstname }} {{ $product->user->lastname }}
					@else
						{{ $product->user->username }}
					@endif
				</a>
				@if ($product->user->is_patron)
						<span class="badges patron vamiddle ml-1">{{ Emoji::gemStone() }}</span>
				@endif
				@if ($product->user->streak > 0)
					<span class="badges streaks vamiddle ml-1">{{ Emoji::fire() }} {{ $product->user->streak }}</span>
				@endif
                @if ($check->isNewUser($product->user->created_at))
					<span class="badges new text-uppercase vamiddle ml-1">New</span>
				@endif
            </span>
		</div>
	</div>

	@if (Auth::id() == $product->user->id)
		<div class="card mt-3" style="width: 18rem;">
		    <div class="card-body">
		        <a href="#" class="btn btn-dark w-100">
		        	{{ Emoji::backhandIndexPointingRight() }} Ask an Update
		        </a>
		        @auth
			        <a href="#" class="btn btn-light2 w-100 mt-2">
			        	Edit product
			        </a>
		    	@endauth
		    </div>
		</div>
	@endif

	<div class="card mt-3" style="width: 18rem;">
		<div class="card-header">Around the web</div>
		<div class="card-body">
            <img class="social-icon" src="/images/icons/producthunt.svg">
			<a class="text-dark vamiddle ml-1" href="https://www.producthunt.com/{{ '@' . $product->producthunt }}" target="_blank">ProductHunt</a>
			<br>
		</div>
	</div>

	<div class="card mt-3" style="width: 18rem;">
		<div class="card-header">Hashtag</div>
		<div class="card-body">
			<span class="name ml-1 font-weight-bold">
				{{ $product->hashtag }}
			</span>
		</div>
	</div>
</div>
