<div class="mb-2">
    <img class="avatar small rounded" src="{{ $product->logo }}">
    <span class="vamiddle popover-title ml-1">{{ $product->name }}</span>
</div>
<b>{{ $product->pitch }}</b>
