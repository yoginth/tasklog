@foreach ($tasks as $date)
    <h5 class="mb-3 mt-3">
        <span class="font-weight-bold">{{ $date[0]->created_at->format('l') }}</span>,
        <span class="font-weight-normal">{{ $date[0]->created_at->format('F j') }}th</span>
    </h5>
    <div class="card todo">
        @foreach (App\Task::where([['done', 0], ['user_id', $date[0]->user->id], ['body', 'like', '%'.$date[0]->hashtag.'%']])
                        ->whereDate('created_at', $date[0]->created_at->format('Y-m-d'))
                        ->get() as $task)
            <li class="list-group-item" id="done-{{ $task->id }}">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="checkbox" class="custom-control-input" value="{{ $task->id }}" id="{{ $task->id }}" 
                        @if (Auth::id() != $task->user->id) disabled @endif
                        @if ($task->done == 1) checked @endif
                        @guest disabled @endguest
                    >
                    <label class="custom-control-label" for="{{ $task->id }}">
                        <span class="body">
                            {!! 
                                Purify::clean(
                                    preg_replace(
                                        '/(?<!\S)#([0-9a-zA-Z]+)/',
                                        '<a class="text-decoration-none hashtag" href="/products/$1">#$1</a>',
                                        $task->body
                                    )
                                )
                            !!}
                        </span>
                    </label>
                    @if (Auth::id() == $task->user->id)
                        <div class="btn-group">
                            <a href="#" class="ml-2 text-black-50" data-toggle="dropdown">
                                <i class="fa fa-sm fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Edit</a>
                                <button type="submit" name="deletetask" value="{{ $task->id }}" class="dropdown-item" href="#">Delete</button>
                            </div>
                        </div>
                    @endif
                    <small class="float-right" title="{{ $task->created_at->format('M d, Y h:ia') }}">{{ $task->created_at->diffForHumans() }}</small>
                    @if ($task->image)
                        <img class="img-fluid rounded border pre-scrollable card-group mt-2 mb-3" src="{{ asset('uploads/images/tasks/' . $task->image) }}" loading="lazy">
                    @endif
                </div>
            </li>
        @endforeach
    </div>
@endforeach
