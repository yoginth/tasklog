<div class="card">
	<div class="user-header">
		<div class="card mt-4 mb-4 ml-3 w-45">
		    <div class="row no-gutters mb-2">
		        <div class="col-auto">
		            <img class="avatar profile rounded-circle mb-3 ml-3 mt-3" src="{{ $product->logo }}">
		        </div>
		        <div class="col">
		            <div class="card-block ml-3 mr-3 mt-2">
						<h3 class="card-title">
							{{ $product->name }}
						</h3>
		                <div class="card-text h6">{{ $product->pitch }}</div>
		            </div>
		        </div>
		    </div>
			<div class="card-footer text-muted profile-footer">
				<div class="row">
					<div class="col-sm-6 text-center v-divider" title="{{ Emoji::calendar() }} Launched {{ $product->created_at->format('F, Y') }}">
				    	{{ Emoji::rocket() }} {{ $product->created_at->format('F Y') }}
				    </div>
					<div class="col-sm-6 text-center" title="{{ Emoji::globeWithMeridians() }} {{ $product->website }}">
						{{ Emoji::globeWithMeridians() }} {{ $product->website }}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="card-footer w-100 text-muted summary-nav">
		<ul class="nav nav-pills nav-fill card-header-pills h6">
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('products/' . $product->slug) ? 'bg-secondary active' : '' }}" href="{{ route('product_overview', [$product->slug]) }}">
					Overview
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('products/' . $product->slug . '/done') ? 'bg-secondary active' : '' }}" href="{{ route('product_done', [$product->slug]) }}">
					{{ $count_done }} Done
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('products/' . $product->slug . '/pending') ? 'bg-secondary active' : '' }}" href="{{ route('product_pending', [$product->slug]) }}">
					{{ $count_pending }} Pending
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('products/' . $product->slug . '/updates') ? 'bg-secondary active' : '' }}" href="{{ route('product_updates', [$product->slug]) }}">
					3 Updates
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('products/' . $product->slug . '/questions') ? 'bg-secondary active' : '' }}" href="{{ route('product_question', [$product->slug]) }}">
					30 Questions
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-dark {{ Request::is('products/' . $product->slug . '/stats') ? 'bg-secondary active' : '' }}" href="{{ route('product_stats', [$product->slug]) }}">
					Stats
				</a>
			</li>
		</ul>
    </div>
</div>
