@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-4 mx-auto">
            <img class="logo auth mb-3 mx-auto d-block" src="/images/logo.svg">
            <h5 class="text-center mb-4">Sign in to Tasklog</h5>
            <div class="card">
                <div class="card-body p-4">
                    <form method="POST" action="{{ route('login') }}">
                        @honeypot
                        @csrf
                        @captcha('en')
                        <div class="form-label-group">
                            <input id="login" type="text" class="form-control{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}" name="login" value="{{ old('username') ?: old('email') }}" placeholder="Username or Email" required autofocus>
                            @if ($errors->has('username') || $errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-label-group mt-3">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="custom-control custom-checkbox mt-3 mb-3">
                            <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="remember">Remember password</label>
                            @if (Route::has('password.request'))
                                <a class="float-right" href="{{ route('password.request') }}">Forgot?</a>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                    </form>
                </div>
            </div>
            <div class="card bg-light text-center mt-3">
                <div class="card-body p-3">
                    New to Tasklog? <a href="/register">Create an account.</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
