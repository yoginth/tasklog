@extends('layouts.app')

@section('content')

<div class="container-fluid">
	@guest
		@include('layouts.banner')
	@endguest
	<div class="row justify-content-center">
		@include('layouts.sidenav')
		<div class="col-md-7 @guest mt-2 @endguest">
			@if ($recent_questions->count())
				@include('home.recent')
			@endif
			@if ($lauched_today->count())
				@include('home.launched')
			@endif
			@auth
				@include('tasks.create')
				@if (Auth::user()->tasks->where('done', 0)->count())
					@include('home.nextup')
				@endif
			@endauth
			@if ($tasks->count())
				<div id="post-data">
					@include('home.tasks')
					<div class="ajax-load text-center" style="display:none">
						<p>Loading More post</p>
					</div>
				</div>
			@else
				@include('layouts.empty')
			@endif
		</div>
		@include('home.sidebar')
	</div>
</div>

@endsection
