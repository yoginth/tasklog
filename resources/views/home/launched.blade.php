<div class="card todo mb-3">
    <div class="card-header">
        {{ Emoji::rocket() }} Launched Today
    </div>
    @foreach ($lauched_today as $product)
        <li class="list-group-item">
            <img class="avatar small rounded mr-2" src="{{ $product->logo }}">
            <a class="text-dark" href="{{ route('product_overview', [$product->slug]) }}">
                <span class="font-weight-bold">{{ $product->name }}</span>
            </a>
            <span>{{ $product->pitch }}</span>
            <a href="{{ route('done', [$product->user->username]) }}">
                <img class="avatar small rounded-circle float-right" src="{{ $product->user->avatar }}">
            </a>
            {{-- Add Follow Button --}}
        </li>
    @endforeach
</div>
