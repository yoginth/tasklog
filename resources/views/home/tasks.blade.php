@foreach ($tasks as $date)
    <h5 class="mb-3 mt-3">
        <span class="font-weight-bold">{{ $date[0]->created_at->format('l') }}</span>,
        <span class="font-weight-normal">{{ $date[0]->created_at->format('F j') }}th</span>
    </h5>
    @foreach ($users as $user)
        @if ($user->tasks()
                    ->where('done', 1)
                    ->whereDate('created_at', '=', $date[0]->created_at->format('Y-m-d'))
                    ->get()
                    ->count())
            <div class="card todo">
                <div class="card-header">
                    <img src="{{ $user->avatar }}" class="avatar small rounded-circle">
                    <span class="name ml-1">
                        @if ($user->firstname)
                            <a class="text-dark" href="{{ route('done', [$user->username]) }}">
                                {{ $user->firstname }} {{ $user->lastname }}
                            </a>
                        @else
                            <a class="text-dark" href="{{ route('done', [$user->username]) }}">
                                {{ $user->username }}
                            </a>
                        @endif
                        @if ($user->is_patron)
                            <span class="badges patron vamiddle ml-1">{{ Emoji::gemStone() }} PATRON</span>
                        @endif
                        @if ($user->streak > 0)
                            <span class="badges streaks vamiddle ml-1">{{ Emoji::fire() }} {{ $user->streak }}</span>
                        @endif
                    </span>
                </div>
                
                @foreach ($user->tasks()
                            ->where('done', 1)
                            ->whereDate('created_at', '=', $date[0]->created_at->format('Y-m-d'))
                            ->latest('updated_at')
                            ->limit(10)
                            ->get() as $task)
                    <li class="list-group-item" id="done-{{ $task->id }}">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="checkbox" class="custom-control-input" value="{{ $task->id }}" id="{{ $task->id }}" 
                                @if (Auth::id() != $user->id) disabled @endif
                                @if ($task->done == 1) checked @endif
                                @guest disabled @endguest
                            >
                            <label class="custom-control-label" for="{{ $task->id }}">
                                <span class="body">
                                    {!! 
                                        Purify::clean(
                                            preg_replace(
                                                '/(?<!\S)#([0-9a-zA-Z]+)/',
                                                '<a class="text-decoration-none hashtag" href="/products/$1">#$1</a>',
                                                $task->body
                                            )
                                        )
                                    !!}
                                </span>
                            </label>
                            @if (Auth::id() == $user->id)
                                <div class="btn-group">
                                    <a href="#" class="ml-2 text-black-50" data-toggle="dropdown">
                                        <i class="fa fa-sm fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">Edit</a>
                                        <button type="submit" name="deletetask" value="{{ $task->id }}" class="dropdown-item" href="#">Delete</button>
                                    </div>
                                </div>
                            @endif
                            <small class="float-right" title="{{ $task->created_at->format('M d, Y h:ia') }}">{{ $task->created_at->diffForHumans() }}</small>
                            @if ($task->image)
                                <img class="img-fluid rounded border pre-scrollable card-group mt-2 mb-3" src="{{ asset('uploads/images/tasks/' . $task->image) }}" loading="lazy">
                            @endif
                        </div>
                    </li>
                @endforeach
            </div>
        @endif
        <div class="mb-3"></div>
    @endforeach
@endforeach
