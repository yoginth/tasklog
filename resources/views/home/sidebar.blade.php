@inject('check', 'App\Http\Controllers\Service\Service')

<div class="col-md-auto">
	@if ($new_users->count())
		<div class="card @guest mt-4 @endguest" style="width:18rem">
			<div class="card-header">{{ Emoji::wavingHand() }} Joined this week</div>
			<div class="card-body">
				@foreach ($new_users as $user)
					<span class="card-text">
						<img src="{{ $user->avatar }}" class="avatar small rounded-circle">
						<a class="ml-1 h6 text-dark text-decoration-none" href="{{ route('done', [$user->username]) }}">
							@if ($user->firstname)
								{{ $user->firstname }} {{ $user->lastname }}
							@else
								{{ $user->username }}
							@endif
						</a>
					</span>
					@if ($user->is_patron)
						<span class="badges patron vamiddle ml-1">{{ Emoji::gemStone() }}</span>
					@endif
					@if ($user->streak > 0)
						<span class="badges streaks vamiddle ml-1">{{ Emoji::fire() }} {{ $user->streak }}</span>
					@endif
					@if ($check->isNewUser($user->created_at))
						<span class="badges new text-uppercase vamiddle ml-1">New</span>
					@endif
					<br>
					<div class="mt-2"></div>
				@endforeach
			</div>
		</div>
	@endif
	<div class="card 
		@if (!$new_users->count()) @guest mt-4 @endguest @endif 
		@if ($new_users->count()) mt-3 @endif" 
		style="width: 18rem;">
		<div class="card-header">{{ Emoji::fire() }} Streaks</div>
	    <div class="card-body">
	    	@if (!$streaks->count())
	    		<span class="card-text text-center">
	    			<div class="h1">{{ Emoji::personShrugging() }}</div>
	    		</span>
	    	@else
		        @foreach ($streaks as $user)
			        <span class="card-text">
			        	<img src="{{ $user->avatar }}" class="avatar small rounded-circle">
			        	<a class="ml-1 h6 text-dark text-decoration-none" href="{{ route('done', [$user->username]) }}">
			        		@if ($user->firstname)
	                            {{ $user->firstname }} {{ $user->lastname }}
	                        @else
	                            {{ $user->username }}
	                        @endif
			        	</a>
					</span>
					@if ($user->is_patron)
						<span class="badges patron vamiddle ml-1">{{ Emoji::gemStone() }}</span>
					@endif
					@if ($user->streak > 0)
						<span class="badges streaks vamiddle ml-1">{{ Emoji::fire() }} {{ $user->streak }}</span>
					@endif
					<br>
					<div class="mt-2"></div>
			    @endforeach
		    @endif
	    </div>
	</div>
</div>
