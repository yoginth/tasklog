<div class="card mb-3 @guest mt-3 @endguest">
    <div class="card-header">{{ Emoji::questionMark() }} Recent Questions</div>
    @foreach ($recent_questions as $question)
    <div class="card-body mb-n4">
        <a class="text-decoration-none" href="{{ route('done', [$question->user->username]) }}">
            <img class="avatar mini rounded-circle" src="{{ $question->user->avatar }}">
        </a>
        <a class="font-weight-bold text-dark text-decoration-none ml-1 vamiddle" href="{{ $question->path() }}">
            {{ $question->title }}
        </a>
        <a class="text-dark text-decoration-none ml-1 vamiddle" href="{{ $question->path() }}">
            @if ($question->answers->count())
                @if ($question->answers->count() == 1)
                    {{ $question->answers->count() }} answer
                @else
                    {{ $question->answers->count() }} answers
                @endif
            @else
                Be the first to answer
            @endif
        </a>
    </div>
    @endforeach
    <div class="mb-4"></div>
</div>
