@if (Auth::check() and !Auth::user()->isSuspended())
    <div class="card todo mb-3">
        <div class="card-header">
            {{ Emoji::rightFacingFist() }} Next up
        </div>
        @foreach (Auth::user()->tasks()->where('done', 0)->limit(5)->latest()->get() as $task)
            <li class="list-group-item" id="next-up-{{ $task->id }}">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="checkbox" class="custom-control-input" value="{{ $task->id }}" id="{{ $task->id }}">
                    <label class="custom-control-label" for="{{ $task->id }}">
                        <span class="body">
                            {!! 
                                Purify::clean(
                                    preg_replace(
                                        '/(?<!\S)#([0-9a-zA-Z]+)/',
                                        '<a class="text-decoration-none hashtag" href="/products/$1">#$1</a>',
                                        $task->body
                                    )
                                )
                            !!}
                        </span>
                    </label>
                    <div class="btn-group">
                        <a href="#" class="ml-2 text-black-50" data-toggle="dropdown">
                            <i class="fa fa-sm fa-ellipsis-h"></i>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Edit</a>
                            <button type="submit" name="deletetask" value="{{ $task->id }}" class="dropdown-item" href="#">Delete</button>
                        </div>
                    </div>
                    <small class="float-right" title="{{ $task->created_at->format('M d, Y h:ia') }}">{{ $task->created_at->diffForHumans() }}</small>
                    @if ($task->image)
                        <img class="img-fluid rounded border pre-scrollable card-group mt-2 mb-3" src="{{ asset('uploads/images/tasks/' . $task->image) }}" loading="lazy">
                    @endif
                </div>
            </li>
        @endforeach
        @if (Auth::user()->tasks->where('done', 0)->count() > 5)
            <li class="list-group-item font-weight-bold">
                <a href="{{ route('pending', [Auth::user()->username]) }}">See all pending todos</a>
            </li>
        @endif
    </div>
@endif
