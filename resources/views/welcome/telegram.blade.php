@extends('layouts.app')

@section('content')

<div class="container-fluid vertical-center">
    <div class="col-3 mx-auto">
        <div class="row text-center justify-content-center">
            <img class="telegram-logo" src="/images/telegram.svg">
            <h3 class="mb-3 mt-3">Say hello to @tasklogbot</h3>
            <p>We use Telegram Messenger to chat about the things we're checking in.</p>
            <p>While most group chats tend to be distracting, with @tasklogbot we make sure everybody stays focused on their work.</p>
            <div>1. <a href="">Download Telegram for free</a></div>
            <div class="mb-3">2. <a href="">Connect your account to @tasklogbot</a></div>
            <a href="{{ route('finish') }}" class="btn btn-success btn-block">Finish setup</a>
        </div>
    </div>
</div>

@endsection
