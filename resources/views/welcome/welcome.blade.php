@extends('layouts.app')

@section('content')

<div class="container-fluid vertical-center">
    <div class="col-3 mx-auto">
        <div class="hello text-center">{{ Emoji::wavingHand() }}</div>
        <div class="row text-center justify-content-center">
            <h3 class="mb-3 mt-3">Welcome to Tasklog</h3>
            <p>Whether you're just starting out or a seasoned entrepreneur, we think you're going to love the free community.</p>
            <p>Before you get started though, let's get you set up and introduce you to Tasklog's core features.</p>
            <a href="{{ route('telegram') }}" class="btn btn-success btn-block">Get started</a>
        </div>
    </div>
</div>

@endsection
