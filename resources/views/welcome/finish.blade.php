@extends('layouts.app')

@section('content')

<div class="container-fluid vertical-center">
    <div class="col-3 mx-auto">
        <div class="hello text-center">{{ Emoji::thumbsUp() }}</div>
        <div class="row text-center justify-content-center">
            <h3 class="mb-3 mt-3">All done!</h3>
            <p>To complete your profile, please tell us a little bit more about yourself.</p>
            <form method="POST" action="/congrats">
				@csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control @error('firstname') is-invalid @enderror" id="firstname" name="firstname" value="{{ Auth::user()->firstname }}" placeholder="First name">
                        @error('firstname')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control @error('lastname') is-invalid @enderror" id="lastname" name="lastname" value="{{ Auth::user()->lastname }}" placeholder="Last name">
                        @error('lastname')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
                    </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control @error('location') is-invalid @enderror" id="location" name="location" value="{{ Auth::user()->location }}" placeholder="Location">
                        @error('location')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
                    </div>
                    <div class="form-group">
                        <textarea class="form-control @error('bio') is-invalid @enderror" id="bio" name="bio" rows="3" placeholder="Tell us something intresting about you...">{{ Auth::user()->bio }}</textarea>
                        @error('bio')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
                    </div>
                    <button type="submit" class="btn btn-success btn-block">All done!</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
