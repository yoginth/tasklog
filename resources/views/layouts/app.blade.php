<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title ?? 'Tasklog' }}</title>
    <link rel="icon" type="image/png" href="/images/favicon.png" sizes="512x512">
    <link rel="apple-touch-icon" type="image/png" href="/images/favicon.png" sizes="512x512">
    <script src="{{ asset('js/app.js') }}" defer data-turbolinks-eval="false"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" data-turbolinks-track="true">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css" rel="stylesheet" data-turbolinks-track="true">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" data-turbolinks-track="true">
</head>
<body>
    <div id="app">
        @if (Auth::check() and Auth::user()->isAdmin())
            @include('layouts.adminbar')
        @endif
        @include('layouts.navbar')
        <main>
            @yield('content')
        </main>
    </div>
</body>
</html>
