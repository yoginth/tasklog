<div class="jumbotron banner mb-0">
	<div>
    	<h1 class="mb-4">Get things done with us.</h1>
    	<a class="btn btn-success" href="/register" role="button">Join today</a>
    	<a class="btn btn-dark" href="/about" role="button">Learn more</a>
    </div>
</div>
