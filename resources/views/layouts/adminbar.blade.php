@php
    $branch = exec('git rev-parse --abbrev-ref HEAD');
    $commit = exec('git rev-parse --short HEAD');
    $full_commit = exec('git rev-parse HEAD');
    $page_load_time = round((microtime(true) - LARAVEL_START) * 1000);
    $count = 0;
    \DB::listen(function ($query) use (&$count) {
        $count++;
    });
@endphp

<nav class="navbar navbar-expand-lg admin-bar">
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            {{ Emoji::babyChick() }}
            <a class="embed ml-2 text-dark text-decoration-none" href="https://gitlab.com/yo/tasklog/tree/{{ $branch }}" title="{{ $branch }}" target="__blank">
                <i class="fas fa-code-branch"></i>
                {{ $branch }}
            </a>
            <a class="embed ml-2 text-dark text-decoration-none" href="https://gitlab.com/yo/tasklog/tree/{{ $commit }}" title="{{ $full_commit }}" target="__blank">
                <i class="fab fa-gitlab"></i>
                {{ $commit }}
            </a>
        </ul>
        <span class="embed mr-2">
            <i class="fab fa-php font-weight-bold"></i>
            {{ phpversion() }}
        </span>
        <span class="embed mr-2">
            <i class="fab fa-laravel font-weight-bold"></i>
            {{ app()->version() }}
        </span>
        <span class="embed mr-2">
            <i class="fas fa-clock"></i>
            {{ $page_load_time }}ms
        </span>
    </div>
</nav>
