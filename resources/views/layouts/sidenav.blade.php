<div class="col-md-auto sidenav">
    <div class="sticky-top pt-4">
        <ul class="nav flex-md-column flex-row justify-content-between">
            <h5>
                <a href="/" class="nav-link mb-2 pl-0">
                    <span class="emoji {{ Request::is('/') ? 'sidebar-active' : '' }}">
                        {{ Emoji::house() }}
                        <span class="sidenav-text">Home</span>
                    </span>
                </a>
            </h5>
            <h5>
                <a href="/products" class="nav-link mb-2 pl-0">
                    <span class="emoji {{ Request::is('products') ? 'sidebar-active' : '' }}">
                        {{ Emoji::partyPopper() }}
                        <span class="sidenav-text">Products</span>
                    </span>
                </a>
            </h5>
            <h5>
                <a href="/questions" class="nav-link mb-2 pl-0">
                    <span class="emoji {{ Request::is('questions') ? 'sidebar-active' : '' }}">
                        {{ Emoji::speechBalloon() }}
                        <span class="sidenav-text">Q&A</span>
                    </span>
                </a>
            </h5>
            <h5>
                <a href="/roasts" class="nav-link mb-2 pl-0">
                    <span class="emoji {{ Request::is('roasts') ? 'sidebar-active' : '' }}">
                        {{ Emoji::fire() }}
                        <span class="sidenav-text">Roasts</span>
                    </span>
                </a>
            </h5>
            <h5>
                <a href="/makers" class="nav-link mb-2 pl-0">
                    <span class="emoji {{ Request::is('makers') ? 'sidebar-active' : '' }}">
                        {{ Emoji::grinningFace() }}
                        <span class="sidenav-text">Makers</span>
                    </span>
                </a></h5>
            <h5>
                <a href="/api" class="nav-link mb-2 pl-0">
                    <span class="emoji {{ Request::is('api') ? 'sidebar-active' : '' }}">
                        {{ Emoji::hammerAndWrench() }}
                        <span class="sidenav-text">API</span>
                    </span>
                </a>
            </h5>
            <div class="dropdown dropup">
                <a class="nav-link pl-0" href="#" data-toggle="dropdown">
                    <span class="emoji">
                        {{ Emoji::backhandIndexPointingRight() }}
                        <span class="sidenav-text">More</span>
                    </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <h5>
                        <a href="/deals" class="nav-link mb-2">
                            <span class="dropdown-emoji">{{ Emoji::wrappedGift() }} Deals</span>
                        </a>
                    </h5>
                    <h5>
                        <a href="/meetups" class="nav-link mb-2">
                            <span class="dropdown-emoji">{{ Emoji::beerMug() }} Meetups</span>
                        </a>
                    </h5>
                    <h5>
                        <a href="/patron" class="nav-link mb-2">
                            <span class="dropdown-emoji">{{ Emoji::gemStone() }} Patron</span>
                        </a>
                    </h5>
                    <h5>
                        <a href="/help" class="nav-link mb-2">
                            <span class="dropdown-emoji">{{ Emoji::thinkingFace() }} Help</span>
                        </a>
                    </h5>
                    <h5>
                        <a href="/widgets" class="nav-link mb-2">
                            <span class="dropdown-emoji">{{ Emoji::gear() }} Widgets</span>
                        </a></h5>
                    <h5>
                        <a href="/open" class="nav-link mb-2">
                            <span class="dropdown-emoji">{{ Emoji::barChart() }} Open</span>
                        </a>
                    </h5>
                </div>
            </div>
        </ul>
    </div>
</div>
