<nav class="navbar navbar-light">
    <a class="navbar-brand" href="{{ route('home') }}">
        <img src="/images/logo.svg" class="d-inline-block align-top logo" alt="Logo">
        <span class="font-weight-bold">Tasklog</span>
    </a>
    @if (Auth::check() and Auth::user()->isBeta())
        <a class="text-decoration-none" href="/beta">{{ Emoji::testTube() }}</a>
    @endif
    @guest
        <div>
            <a class="btn btn-auth" href="/login">Sign in</a>
            @if (Route::has('register'))
                <a class="btn btn-success btn-auth" href="/register">Join</a>
            @endif
        </div>
    @else
        <ul class="navbar-nav ml-auto mr-2">
            <li class="nav-item">
                <a class="text-decoration-none vamiddle" href="https://t.me/tasklogsh" target="_blank">{{ Emoji::speechBalloon() }}</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-2 mr-2">
            <li class="nav-item">
                <span class="badges nav-streaks vamiddle ml-1">
                    {{ Emoji::fire() }} {{ Auth::user()->streak }}
                </span>
            </li>
        </ul>
        <div class="dropdown">
            <a class="nav-link" href="#" data-toggle="dropdown">
                <img class="avatar small rounded-circle mr-1" src="{{ Auth::user()->avatar }}">
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <span class="dropdown-item font-weight-bold">
                    {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}
                    <div>
                        <small><span>@</span>{{ Auth::user()->username }}</small>
                    </div>
                </span>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('done', [Auth::user()->username]) }}">
                    {{ Emoji::bustInSilhouette() }} Profile
                </a>
                <a class="dropdown-item" href="/settings/profile">
                    {{ Emoji::gear() }} Settings
                </a>
                <a class="dropdown-item" href="{{ route('pending', [Auth::user()->username]) }}">
                    {{ Emoji::hourglassNotDone() }} Pending Todos
                </a>
                @if (Auth::user()->isAdmin())
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('admin') }}">
                        {{ Emoji::barChart() }} Admin
                    </a>
                    <a class="dropdown-item" href="https://gitlab.com/tasklog/tasklog">
                        {{ Emoji::nerdFace() }} Source Code
                    </a>
                @endif
                
                <div class="dropdown-divider"></div>
                @if (Auth::user()->isPatron())
                    @if (Auth::user()->dark_mode)
                        <a class="dropdown-item" href="{{ route('dark_mode') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('darkmode').submit();">
                            {{ Emoji::sunWithFace() }} Light mode
                        </a>
                        <form id="darkmode" action="{{ route('dark_mode') }}" method="POST" style="display: none;">
                            @csrf
                            <input type="hidden" name="mode" value="0">
                        </form>
                    @else
                        <a class="dropdown-item" href="{{ route('dark_mode') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('darkmode').submit();">
                            {{ Emoji::firstQuarterMoon() }} Dark mode
                        </a>
                        <form id="darkmode" action="{{ route('dark_mode') }}" method="POST" style="display: none;">
                            @csrf
                            <input type="hidden" name="mode" value="1">
                        </form>
                    @endif
                @endif

                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ Emoji::door() }} Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    @endguest
</nav>
