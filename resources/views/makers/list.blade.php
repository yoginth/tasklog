
@foreach ($dates as $date)
	<div class="card mb-3">
		<div class="card-header">
			<span class="font-weight-bold">{{ $date[0]->created_at->format('l') }}</span>,
			<span class="font-weight-normal">{{ $date[0]->created_at->format('F j, Y') }}</span>
		</div>
		<div class="card-body">
			<div class="row">
				@foreach (App\User::whereDate('created_at', $date[0]->created_at->format('Y-m-d'))
							->orderBy('streak', 'DESC')
							->limit(4)
							->get() as $user)
					<div class="card text-center ml-2 mr-2 mt-4 mb-4" style="width:12rem">
						<div class="card-body mt-2 mb-2">
							<img class="avatar maker rounded-circle mb-4" src="{{ $user->avatar }}">
							<h6 class="card-subtitle mb-2 text-muted">
								@if ($user->firstname)
		                            <a class="text-dark" href="{{ route('done', [$user->username]) }}">
		                                {{ $user->firstname }} {{ $user->lastname }}
		                            </a>
		                        @else
		                            <a class="text-dark" href="{{ route('done', [$user->username]) }}">
		                                {{ $user->username }}
		                            </a>
		                        @endif
							</h6>
							<div class="row justify-content-center mt-3" style="font-size:0.8rem">
								<div class="ml-2 mr-2">
									<a class="text-dark text-decoration-none" href="{{ route('done', [$user->username]) }}">
		                                🔥 {{ $user->streak }}
		                            </a>
								</div>
								<div class="ml-2 mr-2">
									<a class="text-dark text-decoration-none" href="{{ route('done', [$user->username]) }}">
		                                ✅ {{ $user->tasks()->where('done', 1)->count() }}
		                            </a>
								</div>
								<div class="ml-2 mr-2">
									<a class="text-dark text-decoration-none" href="{{ route('pending', [$user->username]) }}">
		                                ⌛ {{ $user->tasks()->where('done', 0)->count() }}
		                            </a>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endforeach
