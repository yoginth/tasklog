@extends('layouts.app')

@section('content')

<div class="container-fluid">
	@guest @include('layouts.banner') @endguest
	<div class="row justify-content-center">
		@include('layouts.sidenav')
		<div class="col-md-7 @guest mt-4 @endguest">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{ Emoji::gemStone() }} Patron</h5>
                    <p class="card-text">
                        <b>Support Tasklog by becoming a Patron.</b>
                        <p>You get the following benefits:</p>
                        <ul>
                            <li>{{ Emoji::redHeart() }} Good karma for helping a bootstrapped startup</li>
                            <li>{{ Emoji::moneyBag() }} Financially commit to staying productive</li>
                            <li>{{ Emoji::fire() }} Enable Dark Mode on the website</li>
                            <li>{{ Emoji::gemStone() }} Show off your support with the Patron badge</li>
                            <li>{{ Emoji::barChart() }} See your shipping stats</li>
                            <li>{{ Emoji::thumbsUp() }} Fund on-going development of the platform</li>
                        </ul>
                        @auth
                            @if (Auth::user()->is_patron)
                                <b>You are already a patron {{ Emoji::partyPopper() }}</b>
                            @else
                                <b>{{ Emoji::locked() }} Secure payment with Stripe</b>
                                <div class="mb-2"></div>
                                <form action="/dopay" method="POST">
                                    @csrf
                                    <script
                                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                        data-key="{{ env('STRIPE_KEY') }}"
                                        data-amount="1000"
                                        data-name="Tasklog Patron"
                                        data-email="{{ Auth::user()->email }}"
                                        data-image="https://tasklog.netlify.com/images/logo.png"
                                        data-currency="usd">
                                    </script>
                                </form>
                                <div class="mb-n2"></div>
                            @endif
                        @endauth
                    </p>
                </div>
            </div>
        </div>
        @include('pages.sidebar')
	</div>
</div>

@endsection
