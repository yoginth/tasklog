<div class="col-md-auto">
	<div class="card text-white bg-success @guest mt-4 @endguest" style="max-width: 18rem;">
		<div class="card-body">
			<h3 class="card-title">
				Browse the products our community is making.
			</h3>
			<a href="#" class="btn btn-dark font-weight-bold">
				{{ Emoji::backhandIndexPointingRight() }} Add your Product
			</a>
		</div>
	</div>
</div>
