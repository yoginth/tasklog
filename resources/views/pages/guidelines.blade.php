@extends('layouts.app')

@section('content')

<div class="container-fluid">
	@guest @include('layouts.banner') @endguest
	<div class="row justify-content-center">
		@include('layouts.sidenav')
		<div class="col-md-7 @guest mt-4 @endguest">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{ Emoji::wrappedGift() }} Guidelines</h5>
                    <p class="card-text">
                        Soon
                    </p>
                </div>
            </div>
        </div>
        @include('pages.sidebar')
	</div>
</div>

@endsection
