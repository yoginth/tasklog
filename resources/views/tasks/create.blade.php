@if (Auth::check() and !Auth::user()->isSuspended())
	<div class="card mb-3">
		<div class="card-header">
			{{ Emoji::newButton() }} Add a todo
		</div>
		<div class="card-body">
			<form method="POST" action="{{ route('tasks.store') }}" enctype="multipart/form-data">
				@csrf
				@honeypot
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<div class="input-group-text">
							<input type="checkbox" id="donestatus" name="donestatus">
						</div>
					</div>
					<input id="body" name="body" type="text" class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" placeholder="New Task" autocomplete="off">
					@error('body')
	                    <span class="invalid-feedback" role="alert">
	                        <strong>{{ $message }}</strong>
	                    </span>
	                @enderror
				</div>
				<input class="mt-1" id="image" name="image" type="file">
				<button class="btn btn-success float-right" type="submit">Add todo</button>
			</form>
		</div>
	</div>
@else
	@include('layouts.suspended')
@endif