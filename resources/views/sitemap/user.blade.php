<?= '<'.'?'.'xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	@foreach($users as $user)
	<url>
		<loc>https://tasklog.sh/{{ '@'.$user->username }}</loc>
		<lastmod>{{ $user->created_at->format('Y-m-d') }}</lastmod>
		<changefreq>monthly</changefreq>
		<priority>0.8</priority>
	</url>
	@endforeach
</urlset>
