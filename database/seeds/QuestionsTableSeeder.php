<?php

use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 20) as $index) {
            DB::table('questions')->insert([
                'user_id'    => $faker->numberBetween($min = 1, $max = 20),
                'title'      => $faker->text($maxNbChars = 35),
                'body'       => $faker->text($maxNbChars = 150),
                'created_at' => Carbon::now(),
            ]);
        }
    }
}
