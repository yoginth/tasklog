<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 1000) as $index) {
            DB::table('tasks')->insert([
                'body'         => $faker->text($maxNbChars = 80),
                'done'         => $faker->boolean,
                'user_id'      => $faker->numberBetween($min = 1, $max = 20),
                'completed_at' => $faker->dateTimeBetween($startDate = '-50 days', $endDate = 'now', $timezone = null),
                'created_at'   => $faker->dateTimeBetween($startDate = '-50 days', $endDate = 'now', $timezone = null),
            ]);
        }
    }
}
