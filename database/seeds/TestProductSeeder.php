<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TestProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'slug'        => 'tasklog',
            'user_id'     => 1,
            'hashtag'     => '#tasklog',
            'name'        => 'Tasklog',
            'pitch'       => 'Get things done with Tasklog',
            'website'     => 'https://tasklog.co',
            'emoji'       => '✅',
            'producthunt' => 'https://producthunt.com/products/tasklog',
            'logo'        => 'https://i.imgur.com/iB3VbIk.png',
            'launched_at' => Carbon::now(),
            'created_at'  => Carbon::now(),
        ]);
    }
}
