<?php

use App\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class TestUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $user = User::where('username', 'user')->firstOrFail();
        $user->delete();
        DB::table('users')->insert([
            'firstname'    => 'User',
            'username'     => 'user',
            'email'        => 'user@user.com',
            'password'     => bcrypt('user'),
            'accent'       => '#8B498F',
            'pattern_id'   => $faker->numberBetween($min = 1, $max = 9),
            'is_admin'     => 0,
            'created_at'   => $faker->dateTimeBetween($startDate = '-50 days', $endDate = 'now', $timezone = null),
        ]);
    }
}
