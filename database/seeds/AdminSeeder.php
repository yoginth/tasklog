<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        DB::table('users')->insert([
            'firstname'    => 'Yoginth',
            'username'     => 'yoginth',
            'email'        => 'me@yoginth.com',
            'password'     => bcrypt('yoginth'),
            'bio'          => 'Designer & Developer • Love to ship products • Music Person',
            'website'      => 'https://yoginth.com',
            'location'     => 'India',
            'twitter'      => 'iamyoginth',
            'telegram'     => 'yoginth',
            'producthunt'  => 'yoginth',
            'accent'       => '#8B498F',
            'pattern_id'   => $faker->numberBetween($min = 1, $max = 9),
            'avatar'       => 'https://yoginth.com/assets/yoginth.png',
            'is_admin'     => 1,
            'is_beta'      => 1,
            'created_at'   => $faker->dateTimeBetween($startDate = '-50 days', $endDate = 'now', $timezone = null),
        ]);
    }
}
