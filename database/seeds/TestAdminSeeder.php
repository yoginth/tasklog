<?php

use App\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class TestAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('username', 'admin')->firstOrFail();
        if ($user === null) {
        } else {
            $user->delete();
        }
        $faker = Faker::create();
        DB::table('users')->insert([
            'firstname'    => 'Admin',
            'username'     => 'admin',
            'email'        => 'admin@admin.com',
            'password'     => bcrypt('admin'),
            'accent'       => '#8B498F',
            'pattern_id'   => $faker->numberBetween($min = 1, $max = 9),
            'is_admin'     => 1,
            'is_beta'      => 1,
            'created_at'   => $faker->dateTimeBetween($startDate = '-50 days', $endDate = 'now', $timezone = null),
        ]);
    }
}
