<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 100) as $index) {
            DB::table('answers')->insert([
                'user_id'     => $faker->numberBetween($min = 1, $max = 20),
                'question_id' => $faker->numberBetween($min = 1, $max = 20),
                'body'        => $faker->text($maxNbChars = 150),
                'claps'       => $faker->numberBetween($min = 1, $max = 10),
                'created_at'  => $faker->dateTimeBetween($startDate = '-50 days', $endDate = 'now', $timezone = null),
            ]);
        }
    }
}
