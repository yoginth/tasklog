<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 30) as $index) {
            DB::table('products')->insert([
                'slug'        => strtolower($faker->firstName.$faker->lastName),
                'user_id'     => $faker->numberBetween($min = 1, $max = 20),
                'hashtag'     => '#'.$faker->firstNameMale,
                'name'        => $faker->firstName,
                'pitch'       => $faker->text($maxNbChars = 50),
                'website'     => $faker->domainName,
                'emoji'       => $faker->emoji,
                'producthunt' => $faker->url,
                'logo'        => 'https://avatar.tobi.sh/'.$faker->userName.'.svg',
                'launched_at' => $faker->dateTimeBetween($startDate = '-50 days', $endDate = 'now', $timezone = null),
                'created_at'  => $faker->dateTimeBetween($startDate = '-50 days', $endDate = 'now', $timezone = null),
            ]);
        }
    }
}
