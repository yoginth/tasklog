<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 20) as $index) {
            DB::table('users')->insert([
                'firstname'    => $faker->firstName,
                'username'     => strtolower($faker->firstName.$faker->lastName),
                'email'        => $faker->email,
                'password'     => bcrypt('yoginth'),
                'twitter'      => $faker->userName,
                'telegram'     => $faker->userName,
                'producthunt'  => $faker->userName,
                'bio'          => $faker->text($maxNbChars = 160),
                'website'      => $faker->url,
                'location'     => $faker->country,
                'accent'       => $faker->hexcolor,
                'pattern_id'   => $faker->numberBetween($min = 1, $max = 9),
                'avatar'       => 'https://avatar.tobi.sh/'.$faker->userName.'.png',
                'created_at'   => $faker->dateTimeBetween($startDate = '-50 days', $endDate = 'now', $timezone = null),
            ]);
        }
    }
}
