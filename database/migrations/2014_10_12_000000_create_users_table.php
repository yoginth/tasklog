<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            // Basics
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('location')->nullable();
            $table->string('bio')->nullable();
            $table->string('avatar')->default('https://i.imgsafe.org/8d/8dd5db6228.gif');
            $table->string('cover')->nullable();
            $table->string('pattern_id')->nullable();
            $table->string('accent')->default('#47E0A0');

            // Social
            $table->string('website')->nullable();
            $table->string('twitter')->nullable();
            $table->string('telegram')->nullable();
            $table->string('producthunt')->nullable();

            // Badges and Permissions
            $table->boolean('is_admin')->default(0);
            $table->boolean('is_beta')->default(0);
            $table->boolean('is_patron')->default(0);
            $table->boolean('dark_mode')->default(0);
            $table->boolean('is_suspended')->default(0);

            // Streaks
            $table->integer('streak')->default(0);
            $table->integer('best_streak')->default(0);

            // Misc
            $table->string('ip_address')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
