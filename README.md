<div align="center">
    <p><img src="public/images/logo.svg" height="70" alt="Tasklog Logo"></p>
    <h1>Tasklog</h1>
    <strong>✅ The Tasklog Web App</strong>
</div>
<br>
<div align="center">
    <a href="https://gitlab.com/yo/tasklog/pipelines">
        <img src="https://gitlab.com/yo/tasklog/badges/master/pipeline.svg?style=flat-square" alt="Build Status">
    </a>
    <a href="https://gitlab.com/yo/tasklog">
        <img src="https://gitlab.com/yo/tasklog/badges/master/coverage.svg?style=flat-square" alt="Code Coverage">
    </a>
    <a href="https://gitlab.styleci.io/repos/14865285">
        <img src="https://gitlab.styleci.io/repos/14865285/shield?branch=master" alt="StyleCI">
    </a>
    <a href="https://www.php.net">
        <img src="https://img.shields.io/badge/PHP-v7.4-blue.svg?logo=php&style=flat-square" alt="PHP Version">
    </a>
    <a href="http://laravel.com">
        <img src="https://img.shields.io/badge/Laravel-v6.x-important.svg?logo=laravel&style=flat-square&longCache=true" alt="Laravel Version">
    </a>
    <a href="https://nodejs.org">
        <img src="https://img.shields.io/badge/Node-v12.x-brightgreen.svg?logo=node.js&style=flat-square&longCache=true" alt="Node Version">
    </a>
    <a href="https://gitlab.com/yo/tasklog/blob/master/LICENSE">
        <img src="https://img.shields.io/badge/license-MIT-green?style=flat-square&longCache=true" alt="MIT License">
    </a>
</div>
<div align="center">
    <br>
    <a href="https://tasklog.sh"><b>tasklog.sh »</b></a>
    <br><br>
    <a href="https://gitlab.com/yo/tasklog/issues/new"><b>Report Bug</b></a>
    •
    <a href="https://gitlab.com/yo/tasklog/issues/new"><b>Request Feature</b></a>
</div>

## About Tasklog

- **✅ Tasks:** All tasks are public and added to our maker profiles.
- **🔥 Streaks:** Earn a shipping streak by completing at least one todo every day, which helps you to stay productive.
- **😀 Makers:** Community of peeps who ships constantly.
- **📦 Products:** Ship your products to Tasklog and make regular updates about the product and even add tasks to them.
- **💬 Q&A:** Get your questions answered and your answers questioned.
- **🎁 Deals:** Discounts and special deals for Tasklog members. Only available to patrons.
- **🍻 Meetups:** Host a simple and beautiful meetup landing page inside Tasklog itself.
- **🚀 Telegram Chat:** Making you more productive.

## Prerequisites

- [PHP](https://www.php.net): please refer to their [installation guide](https://www.php.net/manual/en/install.php).
- [Node](https://nodejs.org): we recommend using [nvm](https://github.com/nvm-sh/nvm) to install the Node version listed on the badge.
- [MySQL](http://www.mysql.com) 8.0 or higher.

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project, ie. https://gitlab.com/yo/tasklog/-/forks/new
2. Clone your forked repository, ie. `git clone https://gitlab.com/<your-username>/tasklog.git`
3. Create your Feature Branch (git checkout -b AmazingFeature)
4. Commit your Changes (git commit -m 'Add some AmazingFeature)
5. Push to the Branch (git push origin AmazingFeature)
6. Open a Pull Request

## Standard Installation

1. Make sure all the prerequisites are installed.
2. Set up your environment variables/secrets in `.env` file

    ```sh
    cp .env.example .env
    ```
3. Run the below commands to install tasklog

    ```sh
    # Install Composer Dependencies
    composer install

    # Install NPM Dependencies
    npm install

    # Build assets for development
    npm run dev
    # Build and minify assets for production
    npm run production
    # Build for dev (With sourcemaps) and watch for changes
    npm run dev

    php artisan key:generate
    php artisan telescope:publish
    php artisan migrate:fresh --seed
    ```
4. That's it! Run `php artisan serve` to start the application and head to `http://localhost:8000`

## Artisan Commands

| Command | Description |
| ------- | ----------- |
| **streaks:calculate** | Calculate all users Streak and Best Streak |
| **streaks:user {userid}** | Calculate given user Streak and Best Streak |

## Development using Docker

This repository ships with a Docker Compose configuration intended for development purposes. It'll build a PHP image with all needed extensions installed and start up a MySQL server and a Node image watching the UI assets.

To get started, make sure you meet the following requirements:

- Docker and Docker Compose are installed
- Your user is part of the `docker` group

If all the conditions are met, you can proceed with the following steps:

1. Make sure **port 8080 is unused** *or else* change `DEV_PORT` to a free port on your host.
2. **Run `chgrp -R docker storage`**. The development container will chown the `storage` directory to the `www-data` user inside the container so Tasklog can write to it. You need to change the group to your host's `docker` group here to not lose access to the `storage` directory.
3. **Run `docker-compose up`** and wait until all database migrations have been done.
4. You can now login with `yoginth` and `yoginth` as password on `localhost:8080` (or another port if specified).

If needed, You'll be able to run any artisan commands via docker-compose like so:

 ```sh
docker-compose run app php artisan list 
```

## Security Vulnerabilities

If you discover a security vulnerability within Tasklog, please send an e-mail to Yoginth via [me@yoginth.com](mailto:me@yoginth.com) or open a Confidential Issue in [GitLab](https://gitlab.com/yo/tasklog/issues/new). All security vulnerabilities will be promptly addressed.

## Author

**Yoginth &lt;me@yoginth.com&gt; (https://yoginth.com)**
* **GitLab:** [@yo](https://github.com/yo)
* **Twitter:** [@iamyoginth](https://twitter.com/iamyoginth)

## Buy us a coffee ☕️

I have developed Tasklog in my free time, because I am passionate about the programming.
Tomorrow I would like to dedicate myself to this, but first, I have a long way to go.

I will continue to do things anyway, but donations are one of the many ways to support what I do.

<a href="https://www.buymeacoffee.com/yoginth">
    <img src="https://tasklog.netlify.com/images/buymeacoffee.svg" width="150" alt="Buy me a Coffee">
</a>

## Crypto

- **BTC:** 39JYsWvZV7Lf9BAzGfoQCdP1iCrmjv8x7r
- **BCH:** qz344x6wt74s0z5tys08zhhl9w8h5j58wqh6lqwyan
- **ETH:** 0x8Ea5215a1B9E7766d55bE4aB5B4F522742DD49a8

## Sponsors

<img src="https://tasklog.netlify.com/images/digitalocean.svg" width="150" alt="DigitalOcean">
<br>
<img src="https://tasklog.netlify.com/images/sentry.svg" width="150" alt="Sentry">

## License

The Tasklog is open-source software licensed under the [**MIT license**](LICENSE).

[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgitlab.com%2Fyo%2Ftasklog.svg?type=large)](https://app.fossa.io/reports/6b1f9e16-9ac8-4a75-9f8e-9bed8f7bc8bf)

🐱🐤

-----

<br>

<div align="center">
    <img width="250px" src="https://i.imgur.com/O1cFKGt.gif">
    <br>
    <strong>Happy Shipping</strong> 🚀
</div>
