<?php

namespace Tests\Feature\Task;

use App\User;
use Tests\TestCase;

class CreateTaskTest extends TestCase
{
    /** @test */
    public function login_user_create_task()
    {
        $user = factory(User::class)->create();
        $response = $this->post('/login', [
            'login' => $user->username,
            'password' => 'password',
        ]);
        $response->assertStatus(302);
        $this->assertAuthenticatedAs($user);

        $task = $this->post('/', [
            'body' => 'Test Task',
        ]);
        $response->assertStatus(302);
    }

    /** @test */
    public function invalid_login_user_create_task()
    {
        $user = factory(User::class)->create();
        $response = $this->post('/login', [
            'login' => $user->username,
            'password' => 'invalid',
        ]);
        $response->assertSessionHasErrors();
        $this->assertGuest();

        $task = $this->post('/', [
            'body' => 'Test Task',
        ]);
        $response->assertSessionHasErrors();
        $this->assertGuest();
    }
}
