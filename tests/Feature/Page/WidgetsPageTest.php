<?php

namespace Tests\Feature\Page;

use Tests\TestCase;

class WidgetsPageTest extends TestCase
{
    /** @test */
    public function widgets_displays_the_widgets_page()
    {
        $response = $this->get('/widgets');

        $response->assertStatus(200);
        $response->assertViewIs('pages.widgets');
    }
}
