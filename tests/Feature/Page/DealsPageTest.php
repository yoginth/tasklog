<?php

namespace Tests\Feature\Page;

use Tests\TestCase;

class DealsPageTest extends TestCase
{
    /** @test */
    public function deals_displays_the_deals_page()
    {
        $response = $this->get('/deals');

        $response->assertStatus(200);
        $response->assertViewIs('pages.deals');
    }
}
