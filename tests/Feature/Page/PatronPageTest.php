<?php

namespace Tests\Feature\Page;

use Tests\TestCase;

class PatronPageTest extends TestCase
{
    /** @test */
    public function patron_displays_the_patron_page()
    {
        $response = $this->get('/patron');

        $response->assertStatus(200);
        $response->assertViewIs('pages.patron');
    }
}
