<?php

namespace Tests\Feature\Page;

use Tests\TestCase;

class TermsPageTest extends TestCase
{
    /** @test */
    public function terms_displays_the_terms_page()
    {
        $response = $this->get('/terms');

        $response->assertStatus(200);
        $response->assertViewIs('pages.terms');
    }
}
