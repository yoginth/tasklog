<?php

namespace Tests\Feature\Page;

use Tests\TestCase;

class GuidelinesPageTest extends TestCase
{
    /** @test */
    public function guidelines_displays_the_guidelines_page()
    {
        $response = $this->get('/guidelines');

        $response->assertStatus(200);
        $response->assertViewIs('pages.guidelines');
    }
}
