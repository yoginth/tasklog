<?php

namespace Tests\Feature\Page;

use Tests\TestCase;

class BetaPageTest extends TestCase
{
    /** @test */
    public function beta_displays_the_beta_page()
    {
        $response = $this->get('/beta');

        $response->assertStatus(302);
    }
}
