<?php

namespace Tests\Feature\Page;

use Tests\TestCase;

class MeetupsPageTest extends TestCase
{
    /** @test */
    public function meetups_displays_the_meetups_page()
    {
        $response = $this->get('/meetups');

        $response->assertStatus(200);
        $response->assertViewIs('pages.meetups');
    }
}
