<?php

namespace Tests\Feature\Page;

use Tests\TestCase;

class OpenPageTest extends TestCase
{
    /** @test */
    public function open_displays_the_open_page()
    {
        $response = $this->get('/open');

        $response->assertStatus(200);
        $response->assertViewIs('pages.open');
    }
}
