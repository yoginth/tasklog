<?php

namespace Tests\Feature\Page;

use Tests\TestCase;

class HelpPageTest extends TestCase
{
    /** @test */
    public function help_displays_the_help_page()
    {
        $response = $this->get('/help');

        $response->assertStatus(200);
        $response->assertViewIs('pages.help');
    }
}
