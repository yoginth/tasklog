<?php

namespace Tests\Feature\Page;

use Tests\TestCase;

class APIPageTest extends TestCase
{
    /** @test */
    public function api_displays_the_api_page()
    {
        $response = $this->get('/api');

        $response->assertStatus(200);
        $response->assertViewIs('pages.api');
    }
}
