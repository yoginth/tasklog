<?php

namespace Tests\Feature;

use Tests\TestCase;

class CommandsTest extends TestCase
{
    /** @test */
    public function calculate_streaks_command()
    {
        $exitCode = \Artisan::call('streaks:calculate');
        $this->assertTrue($exitCode === 0, 'Command executed successfully');
    }
}
