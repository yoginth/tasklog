<?php

namespace Tests\Feature\Admin;

use App\User;
use Tests\TestCase;

class AdminRouteTest extends TestCase
{
    // /** @test */
    // public function view_admin_as_admin_user()
    // {
    //     $exitCode = \Artisan::call('db:seed --class=TestAdminSeeder');
    //     $this->assertTrue($exitCode === 0);

    //     $response = $this->post('/login', [
    //         'login' => 'admin',
    //         'password' => 'admin',
    //     ]);
    //     $response->assertStatus(302);
    //     $response = $this->get('/admin');
    //     $response->assertStatus(200);
    // }

    // /** @test */
    // public function view_admin_as_normal_user()
    // {
    //     $exitCode = \Artisan::call('db:seed --class=TestUserSeeder');
    //     $this->assertTrue($exitCode === 0);

    //     $response = $this->post('/login', [
    //         'login' => 'yoginth',
    //         'password' => 'yoginth',
    //     ]);
    //     $response->assertStatus(302);
    //     $response = $this->get('/admin');
    //     $response->assertStatus(302);
    // }

    /** @test */
    public function view_admin_as_guest()
    {
        $response = $this->get('/admin');
        $response->assertStatus(302);
    }
}
