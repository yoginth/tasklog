<?php

namespace Tests\Feature\Welcome;

use Tests\TestCase;

class WelcomePageTest extends TestCase
{
    /** @test */
    public function welcome_redirects_to_login_page()
    {
        $response = $this->get('/welcome');

        $response->assertStatus(302);
    }
}
