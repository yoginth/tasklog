<?php

namespace Tests\Feature\Welcome;

use Tests\TestCase;

class FinishPageTest extends TestCase
{
    /** @test */
    public function finish_redirects_to_login_page()
    {
        $response = $this->get('/finish');

        $response->assertStatus(302);
    }
}
