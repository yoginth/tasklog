<?php

namespace Tests\Feature\Welcome;

use Tests\TestCase;

class TelegramPageTest extends TestCase
{
    /** @test */
    public function telegram_redirects_to_login_page()
    {
        $response = $this->get('/telegram');

        $response->assertStatus(302);
    }
}
