<?php

namespace Tests\Feature;

use Tests\TestCase;

class HomeTest extends TestCase
{
    /** @test */
    public function home_displays_the_home_page()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertViewIs('home.home');
        $response->assertSee('Get things done with us.');
        $response->assertSee('🔥 Streaks');
        $response->assertSee('Recent Questions');
    }
}
