<?php

namespace Tests\Feature\Auth;

use App\User;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /** @test */
    public function login_displays_the_login_form()
    {
        $response = $this->get(route('login'));

        $response->assertStatus(200);
        $response->assertViewIs('auth.login');
    }

    /** @test */
    public function login_displays_validation_errors()
    {
        $response = $this->post('/login', []);

        $response->assertStatus(302);
        $response->assertSessionHasErrors('username');
        $response->assertSessionHasErrors('password');
    }

    /** @test */
    public function login_user()
    {
        $user = factory(User::class)->create();
        $response = $this->post('/login', [
            'login' => $user->username,
            'password' => 'password',
        ]);
        $response->assertStatus(302);
        $this->assertAuthenticatedAs($user);
    }

    /** @test */
    public function not_login_invalid_user()
    {
        $user = factory(User::class)->create();
        $response = $this->post('/login', [
            'login' => $user->username,
            'password' => 'invalid',
        ]);
        $response->assertRedirect('/');
        $response->assertSessionHasErrors();
        $this->assertGuest();
    }

    /** @test */
    public function logged_user_can_be_logged_out()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->post('/logout');
        $response->assertStatus(302);
        $this->assertGuest();
    }
}
