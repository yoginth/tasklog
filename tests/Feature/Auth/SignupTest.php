<?php

namespace Tests\Feature\Auth;

use App\User;
use Request;
use Tests\TestCase;

class SignupTest extends TestCase
{
    /** @test */
    public function signup_displays_the_signup_form()
    {
        $response = $this->get(route('register'));

        $response->assertStatus(200);
        $response->assertViewIs('auth.register');
    }

    /** @test */
    public function signup_displays_validation_errors()
    {
        $response = $this->post('/register', []);

        $response->assertStatus(302);
        $response->assertSessionHasErrors('username');
        $response->assertSessionHasErrors('email');
        $response->assertSessionHasErrors('password');
    }

    /** @test */
    public function create_user()
    {
        $user = factory(User::class)->make();
        $response = $this->post('register', [
            'username' => $user->username,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password',
            'pattern_id' => rand(1, 9),
            'ip_address' => Request::getClientIp(),
        ]);
        $response->assertStatus(302);
        //$this->assertAuthenticated();
    }

    /** @test */
    public function not_create_invalid_user()
    {
        $user = factory(User::class)->make();
        $response = $this->post('register', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'invalid',
            'pattern_id' => rand(1, 9),
            'ip_address' => Request::getClientIp(),
        ]);
        $response->assertSessionHasErrors();
        $this->assertGuest();
    }
}
