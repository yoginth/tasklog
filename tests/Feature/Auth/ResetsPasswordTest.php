<?php

namespace Tests\Feature\Auth;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Tests\TestCase;

class ResetsPasswordTest extends TestCase
{
    /** @test */
    public function password_request_form_displays_the_password_request_form()
    {
        $response = $this->get('password/reset');
        $response->assertStatus(200);
    }

    /** @test */
    // public function testSendsPasswordResetEmail()
    // {
    //     $user = factory(User::class)->create();
    //     $this->expectsNotification($user, ResetPassword::class);
    //     $response = $this->post('password/email', ['email' => $user->email]);
    //     $response->assertStatus(302);
    // }

    /** @test */
    public function dont_send_reset_if_user_does_not_exist()
    {
        $this->doesntExpectJobs(ResetPassword::class);
        $this->post('password/email', ['email' => 'invalid@email.com']);
    }

    /** @test */
    public function reset_user_password()
    {
        $user = factory(User::class)->create();
        $token = Password::createToken($user);
        $response = $this->post('/password/reset', [
            'token' => $token,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);
        $this->assertTrue(Hash::check('password', $user->fresh()->password));
    }
}
