<?php

namespace Tests\Feature\Product;

use Tests\TestCase;

class ProductsTest extends TestCase
{
    /** @test */
    public function products_displays_the_products_page()
    {
        $response = $this->get('/products');

        $response->assertStatus(200);
        $response->assertViewIs('products.products');
    }
}
