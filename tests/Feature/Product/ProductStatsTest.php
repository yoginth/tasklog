<?php

namespace Tests\Feature\Product;

use Tests\TestCase;

class ProductStatsTest extends TestCase
{
    /** @test */
    public function stats_displays_the_stats_page()
    {
        $response = $this->get('/products/tasklog/stats');

        $response->assertStatus(200);
        $response->assertViewIs('product.stats');
    }
}
