<?php

namespace Tests\Feature\Product;

use Tests\TestCase;

class ProductUpdatesTest extends TestCase
{
    /** @test */
    public function updates_displays_the_updates_page()
    {
        $response = $this->get('/products/tasklog/updates');

        $response->assertStatus(200);
        $response->assertViewIs('product.updates');
    }
}
