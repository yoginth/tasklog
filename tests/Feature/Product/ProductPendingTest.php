<?php

namespace Tests\Feature\Product;

use Tests\TestCase;

class ProductPendingTest extends TestCase
{
    /** @test */
    public function pending_displays_the_pending_page()
    {
        $response = $this->get('/products/tasklog/pending');

        $response->assertStatus(200);
        $response->assertViewIs('product.pending');
    }
}
