<?php

namespace Tests\Feature\Product;

use Tests\TestCase;

class ProductQuestionsTest extends TestCase
{
    /** @test */
    public function questions_displays_the_questions_page()
    {
        $response = $this->get('/products/tasklog/questions');

        $response->assertStatus(200);
        $response->assertViewIs('product.questions');
    }
}
