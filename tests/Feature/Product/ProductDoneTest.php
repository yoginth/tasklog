<?php

namespace Tests\Feature\Product;

use Tests\TestCase;

class ProductDoneTest extends TestCase
{
    /** @test */
    public function done_displays_the_done_page()
    {
        $response = $this->get('/products/tasklog/done');

        $response->assertStatus(200);
        $response->assertViewIs('product.done');
    }
}
