<?php

namespace Tests\Feature\Product;

use Tests\TestCase;

class ProductOverviewTest extends TestCase
{
    /** @test */
    public function overview_displays_the_overview_page()
    {
        $response = $this->get('/products/tasklog');

        $response->assertStatus(200);
        $response->assertViewIs('product.overview');
    }
}
