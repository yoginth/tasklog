<?php

namespace Tests\Feature;

use Tests\TestCase;

class ErrorTest extends TestCase
{
    /** @test */
    public function test_404_page_does_not_show_login()
    {
        $notFound = $this->get('/@404user');
        $notFound->assertStatus(404);
        $notFound->assertDontSeeText('Log in');
    }

    /** @test */
    public function test_404_page()
    {
        $notFound = $this->get('/@404user');
        $notFound->assertStatus(404);
    }
}
