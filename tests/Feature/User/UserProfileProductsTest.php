<?php

namespace Tests\Feature\User;

use App\User;
use Tests\TestCase;

class UserProfileProductsTest extends TestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::all()->last();
    }

    /** @test */
    public function products_displays_the_products_page()
    {
        $response = $this->get('/@'.$this->user->username.'/products');
        $response->assertStatus(200);
        $response->assertViewIs('user.products');
        $response->assertSee($this->user->name);
    }
}
