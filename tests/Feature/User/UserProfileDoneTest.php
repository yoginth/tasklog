<?php

namespace Tests\Feature\User;

use App\User;
use Tests\TestCase;

class UserProfileDoneTest extends TestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::all()->last();
    }

    /** @test */
    public function test_profile_page_shows_name()
    {
        $response = $this->get('/@'.$this->user->username);
        $response->assertStatus(200);
        $response->assertViewIs('user.done');
        $response->assertSee($this->user->name);
    }
}
