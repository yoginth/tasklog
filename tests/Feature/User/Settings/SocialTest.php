<?php

namespace Tests\Feature\User\Settings;

use Tests\TestCase;

class SocialTest extends TestCase
{
    /** @test */
    public function social_redirects_to_login_page()
    {
        $response = $this->get('/settings/social');

        $response->assertStatus(302);
    }
}
