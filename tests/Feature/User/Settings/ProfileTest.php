<?php

namespace Tests\Feature\User\Settings;

use Tests\TestCase;

class ProfileTest extends TestCase
{
    /** @test */
    public function profile_redirects_to_login_page()
    {
        $response = $this->get('/settings/profile');

        $response->assertStatus(302);
    }
}
