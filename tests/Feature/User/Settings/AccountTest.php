<?php

namespace Tests\Feature\User\Settings;

use Tests\TestCase;

class AccountTest extends TestCase
{
    /** @test */
    public function account_redirects_to_login_page()
    {
        $response = $this->get('/settings/account');

        $response->assertStatus(302);
    }
}
