<?php

namespace Tests\Feature\User\Settings;

use Tests\TestCase;

class LabsTest extends TestCase
{
    /** @test */
    public function labs_redirects_to_login_page()
    {
        $response = $this->get('/settings/labs');

        $response->assertStatus(302);
    }
}
