<?php

namespace Tests\Feature\User\Settings;

use Tests\TestCase;

class PasswordTest extends TestCase
{
    /** @test */
    public function password_redirects_to_login_page()
    {
        $response = $this->get('/settings/password');

        $response->assertStatus(302);
    }
}
