<?php

namespace Tests\Feature\User;

use App\User;
use Tests\TestCase;

class UserProfileStatsTest extends TestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::all()->last();
    }

    /** @test */
    public function stats_displays_the_stats_page()
    {
        $response = $this->get('/@'.$this->user->username.'/stats');
        $response->assertStatus(200);
        $response->assertViewIs('user.stats');
        $response->assertSee($this->user->name);
    }
}
