<?php

namespace Tests\Feature\User;

use App\User;
use Tests\TestCase;

class UserProfileQuestionsTest extends TestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::all()->last();
    }

    /** @test */
    public function questions_displays_the_questions_page()
    {
        $response = $this->get('/@'.$this->user->username.'/questions');
        $response->assertStatus(200);
        $response->assertViewIs('user.questions');
        $response->assertSee($this->user->name);
    }
}
