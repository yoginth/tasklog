<?php

namespace Tests\Feature\User;

use App\User;
use Tests\TestCase;

class UserProfileAnswersTest extends TestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::all()->last();
    }

    /** @test */
    public function answers_displays_the_answers_page()
    {
        $response = $this->get('/@'.$this->user->username.'/answers');
        $response->assertStatus(200);
        $response->assertViewIs('user.answers');
        $response->assertSee($this->user->name);
    }
}
