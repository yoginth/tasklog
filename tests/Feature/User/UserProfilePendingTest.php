<?php

namespace Tests\Feature\User;

use App\User;
use Tests\TestCase;

class UserProfilePendingTest extends TestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::all()->last();
    }

    /** @test */
    public function pending_displays_the_pending_page()
    {
        $response = $this->get('/@'.$this->user->username.'/pending');
        $response->assertStatus(200);
        $response->assertViewIs('user.pending');
        $response->assertSee($this->user->name);
    }
}
