<?php

namespace Tests\Feature\Question;

use Tests\TestCase;

class PopularTest extends TestCase
{
    /** @test */
    public function popular_displays_the_popular_page()
    {
        $response = $this->get('/popular');

        $response->assertStatus(200);
        $response->assertViewIs('questions.popular');
    }
}
