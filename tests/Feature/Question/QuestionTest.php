<?php

namespace Tests\Feature\Question;

use Tests\TestCase;

class QuestionTest extends TestCase
{
    /** @test */
    public function question_displays_the_question_page()
    {
        $response = $this->get('/questions/1');

        $response->assertStatus(200);
        $response->assertViewIs('questions.question');
    }
}
