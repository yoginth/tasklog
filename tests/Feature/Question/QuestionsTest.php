<?php

namespace Tests\Feature\Question;

use Tests\TestCase;

class QuestionsTest extends TestCase
{
    /** @test */
    public function questions_displays_the_questions_page()
    {
        $response = $this->get('/questions');

        $response->assertStatus(200);
        $response->assertViewIs('questions.newest');
    }
}
