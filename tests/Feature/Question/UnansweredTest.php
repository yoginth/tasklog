<?php

namespace Tests\Feature\Question;

use Tests\TestCase;

class UnansweredTest extends TestCase
{
    /** @test */
    public function unanswered_displays_the_unanswered_page()
    {
        $response = $this->get('/unanswered');

        $response->assertStatus(200);
        $response->assertViewIs('questions.unanswered');
    }
}
