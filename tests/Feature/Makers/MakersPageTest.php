<?php

namespace Tests\Feature\Makers;

use Tests\TestCase;

class MakersPageTest extends TestCase
{
    /** @test */
    public function makers_displays_the_makers_page()
    {
        $response = $this->get('/makers');

        $response->assertStatus(200);
        $response->assertViewIs('makers.makers');
    }
}
