<?php

namespace App\Http\Controllers\Sitemap;

use App\Http\Controllers\Controller;
use App\User;

class UserSitemapController extends Controller
{
    public function index()
    {
        $users = User::all();

        return response()
            ->view('sitemap/user', ['users' => $users])
            ->header('Content-Type', 'text/xml');
    }
}
