<?php

namespace App\Http\Controllers;

use App\Notifications\NewTask;
use App\Notifications\TaskDone;
use App\Notifications\TaskPending;
use App\Task;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;

class TaskController extends Controller
{
    /**
     * Store a new incomplete task for the authenticated user.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, Task $task)
    {
        // validate the given request
        $data = $this->validate($request, [
            'body'  => ['required', 'string', 'max:255'],
            'image' => ['image', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
        ]);

        $task->user_id = Auth::id();

        if ($request->hasfile('image')) {
            $image = $data['image'];
            $hash = md5($data['image']);
            $filename = $hash.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('uploads/images/tasks/').$filename;
            Image::make($image)->save($location, 60, 'png');
            $task->image = $filename;
        }

        if ($request->input('donestatus') == 'on') {
            $task->done = true;
            $task->completed_at = Carbon::now();
        }

        $task->body = $data['body'];
        $task->save();

        $task->notify(new NewTask($task));

        return back();
    }

    /**
     * Mark the given task as complete and redirect to tasks index.
     *
     * @param \App\Task                $task
     * @param \Illuminate\Http\Request $request
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Task $task, Request $request)
    {
        $data = $this->validate($request, [
            'done' => ['required'],
        ]);

        // check if the authenticated user can complete the task
        $this->authorize('complete', $task);

        // mark the task as complete and save it
        $task->done = $data['done'];
        if ($data['done']) {
            $task->completed_at = Carbon::now();
        }
        $task->save();

        if ($data['done'] == 0) {
            $task->notify(new TaskPending($task));
        } else {
            $task->notify(new TaskDone($task));
        }

        return response()->json([
            'id' => $task->id,
        ]);
    }

    public function destroy(Task $task)
    {
        // check if the authenticated user can complete the task
        $this->authorize('delete', $task);

        if ($task->image) {
            $file_path = public_path('uploads/images/tasks/'.$task->image);
            File::delete($file_path);
        }

        $task = Task::destroy($task->id);

        return response()->json([
            'success' => 'true',
        ]);
    }

    // API /api/task/{id}
    public function get(Task $task, $id)
    {
        $task = Task::where('id', $id)->get();

        return $task;
    }
}
