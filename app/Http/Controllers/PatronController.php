<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class PatronController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function handleonlinepay(Request $request)
    {
        try {
            $unique_id = uniqid();
            Stripe::setApiKey(env('STRIPE_SECRET'));

            $customer = Customer::create([
                'email'  => $request->stripeEmail,
                'source' => $request->stripeToken,
            ]);

            $charge = Charge::create([
                'description' => 'Patron: '.$unique_id,
                'customer'    => $customer->id,
                'amount'      => 1000,
                'currency'    => 'USD',
            ]);

            $user = Auth::user();
            $user->is_patron = true;
            $user->save();

            return back();
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }
}
