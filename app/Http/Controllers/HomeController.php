<?php

namespace App\Http\Controllers;

use App\Product;
use App\Question;
use App\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $users = User::with('latestTask')
            ->get()
            ->sortByDesc('latestTask.updated_at');
        $new_users = User::limit(5)
            ->latest()
            ->whereDate('created_at', '>', Carbon::now()->subDays(7))
            ->get(['firstname', 'lastname', 'avatar', 'streak', 'username', 'is_patron', 'created_at']);
        $streaks = User::limit(10)
            ->where('streak', '>', 0)->orderBy('streak', 'DESC')
            ->get(['firstname', 'lastname', 'avatar', 'streak', 'username', 'is_patron', 'created_at']);
        // Todo
        $paginate = Task::where('done', 1)
            ->whereDate('created_at', Carbon::now()->format('Y-m-d'))
            ->count();
        $tasks = Task::latest()
            ->where('done', 1)
            ->paginate(0)
            ->groupBy(function ($item) {
                return $item->created_at->format('d-M-y');
            });
        $recent_questions = Question::limit(4)
            ->latest()
            ->get();
        $lauched_today = Product::limit(5)
            ->whereDate('launched_at', Carbon::today()->format('Y-m-d'))
            ->latest()
            ->get(['name', 'slug', 'logo', 'pitch', 'user_id']);

        if ($request->ajax()) {
            $view = view('home/tasks', compact('tasks', 'users'))->render();

            return response()->json(['html' => $view]);
        }

        return view('home/home', [
            'users'               => $users,
            'new_users'           => $new_users,
            'streaks'             => $streaks,
            'recent_questions'    => $recent_questions,
            'tasks'               => $tasks,
            'lauched_today'       => $lauched_today,
        ]);
    }
}
