<?php

namespace App\Http\Controllers\Questions;

use App\Http\Controllers\Controller;
use App\Question;

class PopularController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::with('answers')
            ->get()
            ->sortByDesc(function ($question) {
                return $question->answers->count();
            });

        return view('questions.popular', [
            'questions' => $questions,
        ]);
    }
}
