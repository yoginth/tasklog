<?php

namespace App\Http\Controllers\Questions;

use App\Http\Controllers\Controller;
use App\Question;

class UnansweredController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::withCount('answers')
            ->get();

        return view('questions.unanswered', [
            'questions' => $questions,
        ]);
    }
}
