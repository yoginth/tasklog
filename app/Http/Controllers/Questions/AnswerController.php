<?php

namespace App\Http\Controllers\Questions;

use App\Answer;
use App\Http\Controllers\Controller;
use App\Question;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Question $question)
    {
        $question->addAnswer([
            'body'    => request('body'),
            'user_id' => auth()->id(),
        ]);

        return back();
    }

    public function clap($id)
    {
        $answer = Answer::where('id', $id)->first();
        $answer->claps++;
        $answer->save();

        return back();
    }
}
