<?php

namespace App\Http\Controllers\Questions;

use App\Http\Controllers\Controller;
use App\Question;
use App\Tag;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::latest()
        ->get();

        return view('questions.newest', [
            'questions' => $questions,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $question = Question::create([
            'user_id' => auth()->id(),
            'title'   => request('title'),
            'body'    => request('body'),
        ]);

        if ($question) {
            $tagNames = explode(',', $request->get('tags'));
            $tagIds = [];
            foreach ($tagNames as $tagName) {
                $tag = Tag::firstOrCreate(['name' => $tagName]);
                if ($tag) {
                    $tagIds[] = $tag->id;
                }
            }
            $question->tags()->sync($tagIds);
        }

        return redirect($question->path());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Question $question
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        return view('questions.question', [
            'question' => $question,
        ]);
    }

    // API /api/question/{id}
    public function get(Question $question, $id)
    {
        $question = Question::where('id', $id)->get();

        return $question;
    }
}
