<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Service\Service;
use App\Task;
use App\User;
use Illuminate\Http\Request;

class DoneController extends Controller
{
    public function done(Request $request, $username = null)
    {
        $user = User::where('username', $username)
            ->firstorfail();
        $tasks = Task::latest()
            ->where([
                ['done', 1],
                ['user_id', $user->id],
            ])
            ->paginate(0)
            ->groupBy(function ($item) {
                return $item->created_at->format('d-M-y');
            });
        $title = Service::nameOrUsername($user);

        if ($request->ajax()) {
            $view = view('user/content/done', compact('tasks', 'user'))->render();

            return response()->json(['html' => $view]);
        }

        return view('user/done', [
            'title' => $title,
            'user'  => $user,
            'tasks' => $tasks,
        ]);
    }
}
