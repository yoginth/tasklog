<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\User;

class QuestionsController extends Controller
{
    public function questions($username = null)
    {
        $user = User::where('username', $username)
            ->firstorfail();

        return view('user/questions', [
            'user' => $user,
        ]);
    }
}
