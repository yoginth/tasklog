<?php

namespace App\Http\Controllers\User\Settings;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MiscController extends Controller
{
    public function darkMode(Request $request)
    {
        $user = Auth::user();

        $data = $this->validate($request, [
            'mode' => ['nullable', 'string'],
        ]);

        $user->dark_mode = $data['mode'];
        $user->save();

        return back();
    }

    // Update user on /finish page
    public function updateUser(Request $request)
    {
        $user = Auth::user();

        $data = $this->validate($request, [
            'firstname' => ['required', 'string', 'max:20'],
            'lastname'  => ['nullable', 'string', 'max:20'],
            'location'  => ['nullable', 'string', 'max:50'],
            'bio'       => ['nullable', 'string', 'max:160'],
        ]);

        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->location = $data['location'];
        $user->bio = $data['bio'];
        $user->save();

        return view('welcome.congrats');
    }
}
