<?php

namespace App\Http\Controllers\User\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user/settings/social');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $data = $this->validate($request, [
            'website'     => ['nullable', 'string'],
            'telegram'    => ['nullable', 'string'],
            'twitter'     => ['nullable', 'string'],
            'producthunt' => ['nullable', 'string'],
        ]);

        $user->website = $data['website'];
        $user->telegram = $data['telegram'];
        $user->twitter = $data['twitter'];
        $user->producthunt = $data['producthunt'];
        $user->save();

        return back();
    }
}
