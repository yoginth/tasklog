<?php

namespace App\Http\Controllers\User\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user/settings/account');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $data = $this->validate($request, [
            'username' => ['required', 'string', 'alpha_dash', 'max:20'],
            'email'    => ['required', 'string', 'email', 'max:255'],
        ]);

        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->save();

        return back();
    }
}
