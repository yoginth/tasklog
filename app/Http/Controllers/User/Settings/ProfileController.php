<?php

namespace App\Http\Controllers\User\Settings;

use App\Http\Controllers\Controller;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user/settings/profile');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $data = $this->validate($request, [
            'firstname'     => ['required', 'string', 'max:20'],
            'lastname'      => ['nullable', 'string', 'max:20'],
            'location'      => ['nullable', 'string', 'max:50'],
            'bio'           => ['nullable', 'string', 'max:160'],
            'accent'        => ['nullable', 'string', 'max:8'],
            'pattern_id'    => ['nullable', 'string', 'max:1'],
            'avatar'        => ['image', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'cover'         => ['image', 'mimes:jpeg,jpg,png', 'max:10000'],
        ]);

        if ($request->hasfile('avatar')) {
            $file_path = public_path($user->avatar);
            File::delete($file_path);

            $image = $data['avatar'];
            $hash = md5($data['avatar']);
            $filename = $hash.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('uploads/images/avatars/').$filename;
            Image::make($image)->save($location, 60, 'png');
            $user->avatar = '/uploads/images/avatars/'.$filename;
        }

        if ($request->hasfile('cover')) {
            $file_path = public_path($user->cover);
            File::delete($file_path);

            $image = $data['cover'];
            $hash = md5($data['cover']);
            $filename = $hash.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('uploads/images/covers/').$filename;
            Image::make($image)->save($location, 60, 'png');
            $user->cover = '/uploads/images/covers/'.$filename;
        }

        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->location = $data['location'];
        $user->bio = $data['bio'];
        $user->accent = $data['accent'];
        $user->pattern_id = $data['pattern_id'];
        $user->save();

        return back();
    }
}
