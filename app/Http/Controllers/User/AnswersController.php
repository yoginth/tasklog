<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\User;

class AnswersController extends Controller
{
    public function answers($username = null)
    {
        $user = User::where('username', $username)
            ->firstorfail();

        return view('user/answers', [
            'user' => $user,
        ]);
    }
}
