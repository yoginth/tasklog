<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Task;
use App\User;
use Carbon\Carbon;

class StatsController extends Controller
{
    public function stats($username = null)
    {
        $user = User::where('username', $username)
            ->firstorfail();
        $count = 20;

        // Burndown
        $burndown = [];
        $burndown_tasks_count = [];
        $end_date = Carbon::now();
        $date = $user->created_at;
        while (strtotime($date) <= strtotime($end_date)) {
            $burndown[] = $date->addDay()->format('Y-m-d');
            $burndown_tasks_count[] = Task::where([['done', 1], ['user_id', $user->id]])->get()->count();
        }
        // End Burndown

        return view('user/stats', [
            'user' => $user,
            // Burndown
            'burndown'             => '[`'.implode('`,` ', $burndown).'`]',
            'burndown_tasks_count' => '[`'.implode('`,` ', $burndown_tasks_count).'`]',
        ]);
    }
}
