<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Service\Service;
use App\Task;
use App\User;
use Illuminate\Http\Request;

class PendingController extends Controller
{
    public function pending(Request $request, $username = null)
    {
        $user = User::where('username', $username)
            ->firstorfail();
        $tasks = Task::latest()
            ->where([
                ['done', 0],
                ['user_id', $user->id],
            ])
            ->paginate(0)
            ->groupBy(function ($item) {
                return $item->created_at->format('d-M-y');
            });
        $title = Service::nameOrUsername($user);

        if ($request->ajax()) {
            $view = view('user/content/pending', compact('tasks', 'user'))->render();

            return response()->json(['html' => $view]);
        }

        return view('user/pending', [
            'title' => $title,
            'user'  => $user,
            'tasks' => $tasks,
        ]);
    }
}
