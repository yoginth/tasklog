<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\User;

class ProductsController extends Controller
{
    public function products($username = null)
    {
        $user = User::where('username', $username)
            ->firstorfail();

        return view('user/products', [
            'user' => $user,
        ]);
    }
}
