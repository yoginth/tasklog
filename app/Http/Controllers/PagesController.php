<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
    public function terms()
    {
        $title = 'Tasklog Terms';

        return view('pages/terms', [
            'title' => $title,
        ]);
    }

    public function guidelines()
    {
        $title = 'Tasklog Guidelines';

        return view('pages/guidelines', [
            'title' => $title,
        ]);
    }

    public function api()
    {
        $title = 'Tasklog API';

        return view('pages/api', [
            'title' => $title,
        ]);
    }

    public function deals()
    {
        $title = 'Tasklog Deals';

        return view('pages/deals', [
            'title' => $title,
        ]);
    }

    public function help()
    {
        $title = 'Tasklog Help';

        return view('pages/help', [
            'title' => $title,
        ]);
    }

    public function meetups()
    {
        $title = 'Tasklog Meetups';

        return view('pages/meetups', [
            'title' => $title,
        ]);
    }

    public function patron()
    {
        $title = 'Tasklog Patron';

        return view('pages/patron', [
            'title' => $title,
        ]);
    }

    public function widgets()
    {
        $title = 'Tasklog Widgets';

        return view('pages/widgets', [
            'title' => $title,
        ]);
    }

    public function open()
    {
        $title = 'Tasklog Open';

        return view('pages/open', [
            'title' => $title,
        ]);
    }

    public function beta()
    {
        $title = 'Tasklog Beta';

        return view('pages/beta', [
            'title' => $title,
        ]);
    }
}
