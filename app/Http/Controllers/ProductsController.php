<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        $title = 'Products';

        $dates = Product::latest()
            ->paginate(0)
            ->groupBy(function ($product) {
                return $product->created_at->format('d-M-y');
            });

        if ($request->ajax()) {
            $view = view('products/list', compact('dates'))->render();

            return response()->json(['html' => $view]);
        }

        return view('products/products', [
            'title'    => $title,
            'dates'    => $dates,
        ]);
    }

    // Popover
    public function popover($slug = null)
    {
        $product = Product::where('slug', $slug)->firstorfail();

        return view('product/popover', [
            'product' => $product,
        ]);
    }

    // API
    public function get($id = null)
    {
        $product = Product::where('id', $id)->get();

        return $product;
    }
}
