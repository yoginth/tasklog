<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;

class SuspendController extends Controller
{
    public function suspend($id)
    {
        $user = User::where('id', $id)->first();
        if ($user->is_suspended == false) {
            $user->is_suspended = true;
        } else {
            $user->is_suspended = false;
        }
        $user->save();

        return back();
    }
}
