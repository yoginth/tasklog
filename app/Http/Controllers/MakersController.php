<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class MakersController extends Controller
{
    public function index(Request $request)
    {
        $dates = User::latest()
            ->paginate(0)
            ->groupBy(function ($user) {
                return $user->created_at->format('d-M-y');
            });

        if ($request->ajax()) {
            $view = view('makers/list', compact('dates'))->render();

            return response()->json(['html' => $view]);
        }

        return view('makers/makers', [
            'dates' => $dates,
        ]);
    }
}
