<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Service\Service;
use App\Product;

class StatsController extends Controller
{
    public function stats($slug = null)
    {
        $product = Product::where('slug', $slug)
            ->firstorfail();
        $title = 'Products';

        return view('product/stats', [
            'product'       => $product,
            'title'         => $title,
            'count_done'    => Service::countDone($product),
            'count_pending' => Service::countPending($product),
        ]);
    }
}
