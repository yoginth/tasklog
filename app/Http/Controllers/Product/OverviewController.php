<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Service\Service;
use App\Product;
use App\Task;
use Illuminate\Http\Request;

class OverviewController extends Controller
{
    public function overview(Request $request, $slug = null)
    {
        $product = Product::where('slug', $slug)
            ->firstorfail();
        $tasks = Task::latest()
            ->where([
                ['done', 1],
                ['user_id', $product->user->id],
                ['body', 'like', '%'.$product->hashtag.'%'],
            ])
            ->paginate(0)
            ->groupBy(function ($item) {
                return $item->created_at->format('d-M-y');
            });
        $title = 'Products';

        if ($request->ajax()) {
            $view = view('product/content/done', compact('tasks', 'product'))->render();

            return response()->json(['html' => $view]);
        }

        return view('product/overview', [
            'product'       => $product,
            'title'         => $title,
            'tasks'         => $tasks,
            'count_done'    => Service::countDone($product),
            'count_pending' => Service::countPending($product),
        ]);
    }
}
