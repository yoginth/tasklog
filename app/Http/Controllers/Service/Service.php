<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Task;
use Carbon\Carbon;

class Service extends Controller
{
    // Check the User is new or not
    public static function isNewUser($to)
    {
        $from = Carbon::now();
        $diff = $to->diffInDays($from);

        if ($diff < 7) {
            return true;
        } else {
            return false;
        }
    }

    public static function nameOrUsername($user)
    {
        if ($user->firstname) {
            return $user->firstname.' '.$user->lastname.' on Tasklog';
        } else {
            return $user->username.' on Tasklog';
        }
    }

    public static function countDone($product)
    {
        return Task::where([['done', 1], ['user_id', $product->user->id], ['body', 'like', '%'.$product->hashtag.'%']])->count();
    }

    public static function countPending($product)
    {
        return Task::where([['done', 0], ['user_id', $product->user->id], ['body', 'like', '%'.$product->hashtag.'%']])->count();
    }
}
