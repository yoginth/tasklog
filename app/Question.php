<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    public function path()
    {
        return '/questions/'.$this->id;
    }

    /**
     * The relationship to the question's answers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    /**
     * The relationship to the owning user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addAnswer($answer)
    {
        $this->answers()->create($answer);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
