<?php

namespace App\Console\Commands;

use App\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CalculateStreaks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'streaks:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate All Users Streaks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(User $user)
    {
        $iterate = 0;
        $count_users = User::all()->count();
        while ($count_users > $iterate) {
            $iterate++;

            $user = User::where('id', '=', $iterate)->first();
            if ($user === null) {
                $this->error($iterate.' - '.'Not Found');
                $count_users++;
            } else {
                $user = User::where('id', $iterate)->firstOrFail();
                $from = $user->created_at;
                $to = Carbon::today();
                $diff = $to->diffInDays($from);
                $i = 0;
                $streak = 0;
                $best_streak = 0;
                while ($i <= $diff) {
                    $count = Task::where([
                        ['user_id', $user->id],
                        ['done', 1],
                    ])
                    ->whereDate('completed_at', '=', $from->addDay()->format('Y-m-d'))
                    ->count();
                    $i++;
                    if ($count > 0) {
                        $streak++;
                    } else {
                        if ($best_streak < $streak) {
                            $best_streak = $streak;
                        }
                        $streak = 0;
                    }
                    if ($best_streak < $streak) {
                        $best_streak = $streak;
                    }
                }
                $user->streak = $streak;
                $user->best_streak = $best_streak;
                $user->save();
                $this->line($user->id.' - (@'.$user->username.')'.' - [Streaks: '.$streak.', Best Streaks: '.$best_streak.']');
            }
        }
    }
}
