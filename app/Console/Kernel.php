<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use yoginth\laravelTelegramLog\Telegram;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        Telegram::log('Scheduler is running 🕒');
        Telegram::log('Purning Telescope Records 🧹');
        $schedule->command('telescope:prune');
        Telegram::log('Clearing Telescope Records 🧹');
        $schedule->command('telescope:clear');
        Telegram::log('Clearing Application View Cache 🧹');
        $schedule->command('view:clear');
        Telegram::log('Clearing Application Route Cache 🧹');
        $schedule->command('route:clear');
        Telegram::log('Clearing Application Cache 🧹');
        $schedule->command('cache:clear');
        Telegram::log('Clearing all failed Queues 🧹');
        $schedule->command('queue:flush');
        Telegram::log('Remove the compiled class files 🧹');
        $schedule->command('clear-compiled');
        Telegram::log('Calculating User Streaks 🧮');
        $schedule->command('streaks:calculate');
        Telegram::log('Scheduler run completed ✅');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
