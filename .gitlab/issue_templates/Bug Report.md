**Describe the bug**

A clear and concise description of what the bug is.

**Steps To Reproduce**

Steps to reproduce the behavior:
1. Go to '...'
2. Click on '....'
3. Scroll down to '....'
4. See error

**Expected behavior**

A clear and concise description of what you expected to happen.

**Screenshots**

If applicable, add screenshots to help explain your problem.

**Your Configuration (please complete the following information):**

 - Exact Tasklog Commit Hash (Found in adminbar): 
 - PHP Version: 
 - Hosting Method (Nginx/Apache/Docker): 

**Additional context**

Add any other context about the problem here.
