**Describe the feature you'd like**

A clear description of the feature you'd like implemented in Tasklog.

**Describe the benefits this feature would bring to Tasklog users**

Explain the measurable benefits this feature would achieve.

**Additional context**

Add any other context or screenshots about the feature request here.
