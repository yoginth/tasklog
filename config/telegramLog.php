<?php

return [
    // Telegram Bot Token
    'token' => env('TELEGRAM_BOT_TOKEN', ''),

    // Telegram Chat Id
    'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
];
